# README #

*(versão em **PORTUGUÊS** mais abaixo / version en **ESPAÑOL** mas abajo)*
[![Performance em 19/5/2017 com o R.I.S.C.O.](https://bytebucket.org/mmoritz/somo/raw/d67c50179d9f75c567657acb78fae6c2ba288396/photo_2017-05-26_13-39-36.jpg)](https://youtu.be/SHhqdaUqi8Y?t=52s)

![Arms Movement][image 1]

This repository holds what we are calling **SoMo** by now. Created by Tiago Brizolara and Marcos Moritz. *License* is [GPL3][1] .

A paper about it (Portuguese) was published in the 15th Brazillian Symposium of Computational Music: [SoMo: Um Instrumento Musical baseado em Movimento – Características Estéticas e Técnicas e potenciais Usos][5], and an updated version is in SoMo_Instrumento_Musical_Baseado_em_Movimento__SBCM_2015.pdf, located in the main folder of this repository. There you can find many ideas of usage!

You can watch excerpts from a performance [here][1o R.I.S.C.O.] and early tries [here][4].

It uses Eclipse with Processing (www.processing.org) as a library, and Pure Data (https://puredata.info/), a.k.a. Pd ("vanilla" distribution is fine).

## Overview ##

SoMo is designed as a musical instrument to be controlled by movement, with very fast responsivity. As with any, the development of artistic expression passes through becoming familiar with the instrument.

In short, the state of the synthesizer will be given by what is moving in front of the camera (normally, the player). We have a pitch which varies its frequency frenetically between F - W/2 and F + W/2. F is given by the center of the actual moving portion in the video and W is given by the horizontal extent of the moving portion in the video. The remaining details you can read in the [paper][5] (or [updated][6]), besides the code and Pd patch).
![Hand Movement][image 2]

## Requirements ##

* A webcam. At 60 FPS the experience is very good. PS3Eye camaera is low cost and have Linux drivers to achieve more than 100 FPS...
* Audio:
    * Install [Pure Data][7], it's free and lightweight
* Video:
    * Java 7 or greater
    * *If you want to build the Java app yourself, read the section Developers below*
* Operating System:
	* We built and tested in *Windows 7* and *Linux Ubuntu 16.04.1*. It's expected that it'll work on MacOS too.

## Getting Started ##

### Users

Download SoMo1.1.0.-video.jar (or whatever version) and OsSo.pd, they are in [https://bitbucket.org/mmoritz/somo/src](https://bitbucket.org/mmoritz/somo/src)

#### Audio: Pd

Open the program pd and then open the file *OsSo.pd* from the SoMo root folder. To enable the sound, go to the `Media->DSP On` on Pd's main window.

#### Video: Running the .jar

In Windows, just open the file and it should run.    
In Linux, open a Terminal, go to the folder where the file is located and enter

	`java -jar SoMo1.1.0.-video.jar`
(or whatever the number version on the file is)

### Users or Developers

Inside your .jar file, you'll find *config.json*, which you can customize:
```{	
	"videoResolution_or_videoDeviceIndex": "QVGA",
	"displayBodyPolygons": "1",
	"pixelChangeThreshold": 60,
	"sectorsMatrixSize": {
		"rows": 48,
		"columns": 80
	},
	"oscMessageName": "/test",
	"oscIpAddress": "localhost",
	"oscPort": 56789,
	"skippingPixels": {
		"x": 2,
		"y": 2
	},
	"maxSectorAccumulation": 60,
	"sectorAccumulationFraction": 10,
	"oscMessageName": "/osso",
	"oscMessageName_manual": "/osso_manual",
	"oscLocalhostIpAddress": "localhost",
	"oscLocalhostPort": 56789,
	"oscWatcherIpAddress": "10.0.0.120",
	"oscWatcherPort": 8080,
	"oscWatcherMsgName_100_over_FPS": "/vol4",
	"oscWatcherMsgName_AccFraction_over_30": "/vol1",
	"oscWatcherMsgName_PixChangeThreshold_over_100": "/vol2",
	"oscWatcherMsgName_Distorter": "/solo6",
	"oscWatcherMsgName_Uivo": "/mute6",
	"oscWatcherMsgName_LevelMultiplier": "/vol6"
}
```
* *videoResolution_or_videoDeviceIndex*: affects the video device to be used. Even if you have just one camera (for example, your laptop's built-in cam), many configurations of it are available and each will be listed as a video device on your operating system. "QVGA" means 240x480 resolution; "VGA" is 640x480, etc. (all options are described in src/VideoResolutions.java). The program will choose the fastest (high frame rate - frames per second) device of the resolution informed (or of the next resolution available, if all previous devices tested happen to not stream video appropriately). **Yes, we are making your life easier ;). Maybe you'll never need to edit this file...** As we want a device configuration which is fast, probably we won't have good options in higher resolutions. *In our experience we are using PS3Eye camera, which can achieve more than 100 FPS at QVGA resolution under Linux...!*
You can still choose manually which device you want. You just set the number of the device ("0", "9", "56", whatever. Don't forget the quotes), instead of the resolution.
* *displayBodyPolygons*: "1" = transparent red polygon covering movement area will show up in screen; "0" = no polygon will be shown. The polygon is a visual indicator of the information to be send to the sound synthesis, but you may not like its appearance...
* *pixelChangeThreshold*: is the threshold to changes in video. You may want to increase the threshold if your video is noisy and thus accusing too much false movements, for example.
* *sectorsMatrixSize*: is the number of rows and columns of the grid of regions that the program is going to analyze. You can think of them as big pixels. Looking at these values, we see that we can really choose fast devices configurations in spite of small resolution. Every time ou run the program you'll see the devices and their number printed in the console.
* *oscMessageName*, *oscIpAddress*, *oscPort*: they control the OSC (a network protocol - a kind of UDP) parameters to communicate with the synthesizer, which is the running Pure Data (OsSo.pd) program. OsSo is configured with that parameters, so, if you make changes here, you'll have to change OsSo OSC parameters too. "localhost" means we are communicating via the very own computer, not the internet.
* *skippingPixels* and *maxSectorAccumulation*: internal stuff that we are probably going to take off this configuration file...

### Developers

1. Install [Eclipse][8]
2. [Download][2] the repository or `git clone https://your_account@bitbucket.org/mmoritz/somo.git`

#### Import the project CameraWave into Eclipse ###
If you're not familiar with Eclipse, follow the instructions [here][3] to get the project into your Eclipse workspace.

#### Java Compiler ###
Make sure the compiler level for the project is set to be **JavaSE-1.7 or higher**. To check this out, right click on the project's root folder on *Project Explorer* and select *Properties*. The project's properties window will open. Click on the *Java Compiler* item on the side-bar. Make sure the project will be compiled with the right version of *JavaSE*.

## Thanks! ##

Thanks to Élder Sereni for early tests and thoughts, and to Dimi and Fabio for tests, thoughts and letting the instrument be used on Ohrwurm exhibition.

## Who do I talk to? ##

marcosmoritz at gmail / tiago dot brizolara at gmail

-----------------------------

# LEIA-ME #
[![Performance em 19/5/2017 com o R.I.S.C.O.](https://bytebucket.org/mmoritz/somo/raw/d67c50179d9f75c567657acb78fae6c2ba288396/photo_2017-05-26_13-39-36.jpg)](https://youtu.be/SHhqdaUqi8Y?t=52s)
![Arms Movement][image 1]

Este repositório contém o que estamos chamando SoMo por agora. Criado por Tiago Brizolara e Marcos Moritz.

Um *paper* sobre o instrumento foi publicado no 15o Simpósio Brasileiro de Computação Musical : [SoMo: Um Instrumento Musical baseado em Movimento – Características Estéticas e Técnicas e potenciais Usos][5] e uma versão atualizada está no SoMo_Instrumento_Musical_Baseado_em_Movimento__SBCM_2015.pdf, na pasta principal do repositório. Nele você pode encontrar várias dicas de uso!

Você pode assistir a uma experiência inicial no [Youtube][4].
Você pode assistir trechos de uma performance [aqui][1o R.I.S.C.O.] e primeiras experiências [aqui][4].

Usamos Eclipse com Processing como uma biblioteca, e Pd (Pure Data. A distribuição "vanilla" é suficiente).

## Overview ##

SoMo foi concebido como um instrumento musical para ser controlado pelo movimento. Tal como acontece com qualquer instrumento, o desenvolvimento da expressão artística passa pelo tornar-se familiar com ele.

Em resumo, o estado do sintetizador será dado pelo que está se movendo em frente à câmera (normalmente, o executante). Temos uma altura musical (pitch) que varia sua frequência freneticamente entre F - W/2 e F + W/2. F é dado pelo centro da porção que está se movendo no vídeo e W pela extensão horizontal dessa mesma porção. Os detalhes restantes você pode ler no [paper][5] ([atualizado][6]), além do código e do Patch de Pd.

![Hand Movement][image 2]

## Requirementos ##

* Uma webcam. A 60 FPS (quadros por segundo) a experiência é muito boa. PS3Eye tem baixo custo e drivers no Linux para alcançar mais de 100 FPS...
* Áudio:
    * Instale o [Pure Data][7], é gratuito e leve.
* Vídeo:
    * Java 7 or superior
    * *Se você mesmo quer fazer o build, leia a seção Developers, mais abaixo*
* Sistema operacional:
	* Fizemos o build e testamos em *Windows 7* e *Linux Ubuntu 16.04.1*. Espera-se que funcione no MacOS também.

## Getting Started ##

### Usuários

Baixe SoMo1.1.0.-video.jar (ou a versão que estiver disponível) e OsSo.pd, eles estão em [https://bitbucket.org/mmoritz/somo/src](https://bitbucket.org/mmoritz/somo/src)

#### Áudio: Pd

Abra o programa pd e então abra o arquivo *OsSo.pd* da pasta raíz do projeto do SoMo. Para habilitar o áudio, marke o botão `Media->DSP On` na janela princial do Pd.

#### Vídeo: Rodando o .jar

No Windows, basta abrir o arquivo.    
No Linux, abra um Terminal, vá para a pasta onde o arquivo se encontra e entre com 

	`java -jar SoMo1.1.0.-video.jar`
(ou qualquer que seja a versão numérica atual)

### Usuários ou Desenvolvedores

Dentro dos eu arquivo .jar file, você irá encontrar *config.json*, o qual você pode customizar:
```{	
	"videoResolution_or_videoDeviceIndex": "QVGA",
	"displayBodyPolygons": "1",
	"pixelChangeThreshold": 60,
	"sectorsMatrixSize": {
		"rows": 48,
		"columns": 80
	},
	"oscMessageName": "/test",
	"oscIpAddress": "localhost",
	"oscPort": 56789,
	"skippingPixels": {
		"x": 2,
		"y": 2
	},
	"maxSectorAccumulation": 60,
	"sectorAccumulationFraction": 10,
	"oscMessageName": "/osso",
	"oscMessageName_manual": "/osso_manual",
	"oscLocalhostIpAddress": "localhost",
	"oscLocalhostPort": 56789,
	"oscWatcherIpAddress": "10.0.0.120",
	"oscWatcherPort": 8080,
	"oscWatcherMsgName_100_over_FPS": "/vol4",
	"oscWatcherMsgName_AccFraction_over_30": "/vol1",
	"oscWatcherMsgName_PixChangeThreshold_over_100": "/vol2",
	"oscWatcherMsgName_Distorter": "/solo6",
	"oscWatcherMsgName_Uivo": "/mute6",
	"oscWatcherMsgName_LevelMultiplier": "/vol6"
}
```
* *videoResolution_or_videoDeviceIndex*: afeta a escolha do dispositivo de áudio. Memso que você tenha apenas uma câmera (por exemplo, a webcam embutida no seu laptop), muitas configurações são possíveis e cada uma aprecerá como um dispositivi separado no seu sistema operacional. "QVGA" significa resolução 240x480; "VGA" é 640x480, etc. (todas as opções estão descritas em src/VideoResolutions.java). O programa irá escolher o dispositivo mais rápido (alta taxa de quadros - frames per second) para a resolução informada (ou para a próxima resolução disponível, se acontecer de todos os dispositivos anteriores testados não estiverem transmitindo vídeo adequadamente). **Sim, estamos facilitando a sua vida ;). Talvez você não precise nem abrir esse arquivo vez alguma...** Como queremos uma configuração de dispositivo que seja rápida, provavelmente não teremos boas opções em resoluções maiores. *Na nossa experiência estamos usando uma câmera PS3Eye, que pode alcançae mais de 100 FPS em resolução QVGA no Linux...!*
Você ainda pode escolher manualmente que dispositivo voce quer. Basta informar o número do dispositivo ("0", "9", "56", qual seja. Não esqueça das aspas), ao invés da resolução.
* *displayBodyPolygons*: "1" = um polígono vermelho com transparência vai aparecer sobre a área em movimento no vídeo; "0" = não aparecerá polígono. Esse polígono é uma indicação visual da informação a ser enviada para a síntese de áudio, mas talvez você não deseje que ele apareça...
* *pixelChangeThreshold*: é o limiar para mudanças no vídeo. Você pode querer aumentar o threshold se seu video é ruidoso, acusando muitos falsos movimentos, por exemplo.
* *sectorsMatrixSize*: é o número de linhas e colunas da grade de regiões que o programa vai analisar. Você pode pensar nessas regiões como grandes pixels. Olhando para esses valores, vemos que podemos mesmo escolher dispsitivos rápiudos em detrimento da resolução. Toda vez que você rodar o programa verá uma lista com os dispositivos e seus números.
* *oscMessageName*, *oscIpAddress*, *oscPort*: controlam os parâmetros do OSC (um protocolo de rede - um tipo de UDP) para comunicação com o sintetizador, que está rodando em Pure Data (OsSo.pd). OsSo está configurado com esses parâmetros, assim, se você alterá-los aqui, terá que alterar os parâmetros de OSC do OsSo também. "localhost" significa que estamos comunicando via o próprio computador, não pela internet.
* *skippingPixels* e *maxSectorAccumulation*: coisas internas que provavelemtren serão retiradas do arquivo de configurações em breve...

### Desenvolvedores

1. Instale [Eclipse][8]
2. [Baixe][2] o repositório ou `git clone https://your_account@bitbucket.org/mmoritz/somo.git`

### Importe o projeto CameraWave no Eclipse ###
Se você não está familiarizado com o Eclipse, siga as instruções encontradas [aqui][3] para incluir o projeto na área de trabalho do seu Eclipse.

### Compilador Java ###
Certifique-se de que a versão do compilador que será utilizado no projeto é a JavaSE-1.7 ou superior. Para isso você vai precisar clicar com o botão direito na pasta raiz do projeto no *Project Explorer* e selecionar *Properties*. A janela de propriedades do projeto vai abrir e você deve selecionar *Java Compiler* na coluna da esquerda. Certifique-se de que o projeto será compilado por uma versão adequada do JavaSE.

## Obrigado! ##

Obrigado a Élder Sereni por testes iniciais e ideias, e a Dimi e Fabio por testes, ideias e por permitir o uso do instrumento na performance Ohrwurm.

## Com quem posso falar? ##

marcosmoritz at gmail / tiago dot brizolara at gmail

-----------------------

# LÉAME #

*(versão em **PORTUGUÊS** mais abaixo)*
[![Performance em 19/5/2017 com o R.I.S.C.O.](https://bytebucket.org/mmoritz/somo/raw/d67c50179d9f75c567657acb78fae6c2ba288396/photo_2017-05-26_13-39-36.jpg)](https://youtu.be/SHhqdaUqi8Y?t=52s)
![Arms Movement][image 1]

Este repositorio guarda lo que estamos llamando de SoMo por ahora. Creado por Tiago Brizolara y Marcos Moritz.

Un paper sobre el (en Portugués) se ha publicado en el 15o Simpósio Brasileiro de Computação Musical: [SoMo: Um Instrumento Musical baseado em Movimento – Características Estéticas e Técnicas e potenciais Usos][5]. Ahí puedes encontrar variadas formas de uso!

Se puede ver partes de una performance [aqui][1o R.I.S.C.O.] y primeras experiencias [aqui][4].

Es usado Eclipse con Processing como una biblioteca, y Pd (Pure Data. La distribuición "vanilla" es suficiente).

## Overview ##

### Concepto ###

SoMo está diseñado como un instrumento musical para ser controlado por el movimiento. Como con cualquier, el desarrollo de la expresión artística pasa por familiarizarse con el instrumento.

En resumen, el estado del sintetizador será dado por lo que se mueve en frente a la cámara (normalmente, el ejecutante). Tenemos un pitch (altura musical) que varia su frecuencia freneticamente entre F - W/2 y F + W/2. F es dado por el centro de la actua porción que se mueve en el videoy W es dado por la cobertura horizontal de esa porción que se mueve. Los demás detalles se pueden leer en el [paper][5] (además del código y patch de Pd).
![Hand Movement][image 2]

## Requirements ##

* Una webcam. A 60 FPS (cuadros por segundo) la experiencia es muy buena. PS3Eye tiene bajo costo y drivers Linux para alcanzar mas de 100 FPS...
* Audio:
    * Instalar [Pure Data][7], es gratuito.
* Video:
    * Java 7 o superior
    * *Si deseas vos mismo hacer el build, lee la sección Desarrolladores más abajo*
* Sistema operacional:
	* Hicemos el build y testamos en *Windows 7* y *Linux Ubuntu 16.04.1*. Se espera que funcione en MacOs también.

## Getting Started ##

### Usuarios

Descarga SoMo1.1.0.-video.jar (o la versión que sea) y OsSo.pd, ellos están en [https://bitbucket.org/mmoritz/somo/src](https://bitbucket.org/mmoritz/somo/src)

#### Audio: Pd

Abre el programa pd y de ahí el archivo *OsSo.pd* de la la carpeta principal de SoMo. Para habilitar el sonido, chequea `Media->DSP On` en la ventana principal de Pure Data.

#### Video: Running the .jar

En Windows, simplesmente abre el archivo y debe iniciar.    
En Linux, abre la Terminal va a la carpeta adonde está el archivo y entra con

	`java -jar SoMo1.1.0.-video.jar`
(o el número de la versión que sea)

### Users or Developers

Adentro del archivo .jar file está *config.json*, lo cual puedes customizar:
```{	
	"videoResolution_or_videoDeviceIndex": "QVGA",
	"displayBodyPolygons": "1",
	"pixelChangeThreshold": 60,
	"sectorsMatrixSize": {
		"rows": 48,
		"columns": 80
	},
	"oscMessageName": "/test",
	"oscIpAddress": "localhost",
	"oscPort": 56789,
	"skippingPixels": {
		"x": 2,
		"y": 2
	},
	"maxSectorAccumulation": 60,
	"sectorAccumulationFraction": 10,
	"oscMessageName": "/osso",
	"oscMessageName_manual": "/osso_manual",
	"oscLocalhostIpAddress": "localhost",
	"oscLocalhostPort": 56789,
	"oscWatcherIpAddress": "10.0.0.120",
	"oscWatcherPort": 8080,
	"oscWatcherMsgName_100_over_FPS": "/vol4",
	"oscWatcherMsgName_AccFraction_over_30": "/vol1",
	"oscWatcherMsgName_PixChangeThreshold_over_100": "/vol2",
	"oscWatcherMsgName_Distorter": "/solo6",
	"oscWatcherMsgName_Uivo": "/mute6",
	"oscWatcherMsgName_LevelMultiplier": "/vol6"
}
```
* *videoResolution_or_videoDeviceIndex*: afecta la elección del dispositivo de video a ser usado. Mismo si tienes apenas una cámera (por ejemplo, tu cámera en el laptop), muchas configuraciones de ella estarán disponibles y cada una va a estar listada como un dispositivo en separado en el sistema operativo. "QVGA" significa 240x480 de resolución; "VGA" es 640x480, etc. (las opciones todas están descritas en src/VideoResolutions.java). El programa va a elegir el dispositivo más rápido (alta velocidad de quadros - frames per second) para la resolución informada (o de la próxima resolución si para esa todos los dispositivos fallaren). **Si, estamos haciendo tu vida más facil ;).Tal vez ni necesites editar ese archivo alguna vez...** Como queremos una configuración que sea veloz, probablemente no tendremos opciones a altas resoluciones. *En nuestra experiencia estamos usando una camera PS3Eye, que puede lograr más de 100 FPS en resolución QVGA en Linux...!*
Todavia puedes elegir manualmente el dispostivo que quieras. Simplesmente usas el número del dispositivo ("0", "9", "56", lo que sea. No te olvides de las comillas), al revés de la resolución. Siempre que correres el programa, vas a ver escrito en la consola el número de cada dispositivo.
* *displayBodyPolygons*: "1" = un polígono rojo con transparencia cubriendo el área de movimiento aparecerá en la pantalla; "0" = no será mostrado el polígono. Ese polígono es una indicación visual de la información a ser enviada para la síntesis de áudio, pero talvez a vos no te guste la aparencia...
* *pixelChangeThreshold*: es el limiar de detección de cambios en el video. Puedes querer aumentar el threshold si tu video es muy ruidoso y está acusando falsos moviemientos.
* *sectorsMatrixSize*: es el número  de líneas y columnas de la cuadrícula de regiones que el programa va a analisar. Puedes pensar en ellas como grandes pixels. Viendo esos valores, vemos que podemos realmente elegir bajas resoluciones para privilegiar altas FPS.
* *oscMessageName*, *oscIpAddress*, *oscPort*: controlan los parámetros de la OSC (un protocolo de red - un tipo de UDP) para comunicar con el sintetizador, que está corriendo en el programa de Pure Data (OsSo.pd). OsSo está configurado con esos parámetros, asi que, si los cambias aqui, tendrás que cambiarlos allá también. "localhost" significa que estamos comunicando por la própria computadora, no por la internet.
* *skippingPixels* and *maxSectorAccumulation*: cosas internas que probablemente vamos a retirar de este archivo de configuración...

### Developers

1. Instala [Eclipse][8]
2. [Baja][2] el repositorio o `git clone https://your_account@bitbucket.org/mmoritz/somo.git`

#### Import the project CameraWave into Eclipse ###
Si no eres familiar a Eclipse, sigue las instrucciones [acá][3] para poner el projecto en tu workspace de Eclipse.

#### Java Compiler ###
Asegurate que el compiler level para el proyecto está como **JavaSE-1.7 o mayor**. Para chequear eso, haga click con botón derecho en la carpeta raíz del proyecto en *Project Explorer* y selecciona *Properties*. The ventana de la aplicación abrirá. Haga click en el item *Java Compiler* en la barra lateral. Asegurate ue el proyecto será compilado con la versión correcta de la *JavaSE*.

## Gracias! ##

Gracias a Élder Sereni por testes iniciales y ideas, y a Dimi y Fabio por testes, ideas y nos permitir usar el instrumento en la performance Ohrwurm.

## Con quien puedo hablar? ##

marcosmoritz at gmail / tiago dot brizolara at gmail




[//]: # (Images)
[image 1]: https://bitbucket.org/repo/rXoKLa/images/3498023119-movement_sonifier_1.png "Early experience image 1"
[image 2]: https://bitbucket.org/repo/rXoKLa/images/2195741334-movement_sonifier_2.png "Early experience image 2"

[//]: # (Videos)
[1o R.I.S.C.O.]: https://www.youtube.com/watch?v=BFd6oPTuuDI "SoMo no 1o R.I.S.C.O."


[//]: # (Links)
[1]: https://bitbucket.org/mmoritz/movement-sonifier/src/fbed8ed0cd033b11eaa7ed1197ed947865c1b915/LICENSE?at=master        "License"
[2]: https://bitbucket.org/mmoritz/movement-sonifier/downloads "Download repository"
[3]: http://help.eclipse.org/juno/index.jsp?topic=%2Forg.eclipse.platform.doc.user%2Ftasks%2Ftasks-importproject.htm "Importing a project in Eclipse"
[4]: https://www.youtube.com/watch?v=2N8no-J0ShY "Youtube early experiences"
[5]: http://compmus.ime.usp.br/sbcm2015/files/proceedings-print.pdf "Published paper"
[6]: https://bitbucket.org/mmoritz/somo/src/4098548aa3b0c2484e863334c17136dfd76a5275/SoMo_Instrumento_Musical_Baseado_em_Movimento__SBCM_2015.pdf?at=master "Updated paper"
[7]: https://puredata.info/downloads "Download Pure Data"
[8]: https://www.eclipse.org/downloads/ "Download Eclipse"