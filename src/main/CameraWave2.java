package main;

import body.BodyManager;
import processing.core.PApplet;
import sectors.AnalysisResult;
import sectors.AnalysisSector;
import sectors.MatrixSize;
import sectors.Point;
import sectors.SectorBuilder;
import sectors.SectorUtils;

@SuppressWarnings("serial")
public class CameraWave2 extends PApplet {


	public static final int 	CAMERA_CAPTURE_INDEX	= 0;
	public static final boolean DEBUG_SECTOR_COLORS 	= true;
	public static final boolean PLAIN_BACKGROUND 		= true;
	
	int backgroundColor = color(0,0,0);
	
	CameraCapture mCamera;
	
	MatrixSize sectorsMatrixSize = new MatrixSize(10, 10);
	Point<Integer> skippingPixels = new Point<>(2, 2);
	Point<Integer> screenSize;	

	int aux;

	// random colors
	int randomColor1, randomColor2;

	BodyManager mBodyManager;

	public void setup(){
		
		mCamera = new CameraCapture(this, CAMERA_CAPTURE_INDEX, CameraCapture.BLACKLIST_RESET);
		screenSize = mCamera.getScreenSize();
		
		size(screenSize.x, screenSize.y);		
		rectMode(CENTER);
		
		mCamera.read();

		SectorBuilder.createSectors(screenSize, sectorsMatrixSize, skippingPixels, processing.core.PConstants.HSB, this);
		
		randomColor1 = color(255, 255, 0, 255);
		randomColor2 = color(0, 255, 0, 90);
				
		int numberOfSectors = AnalysisSector.rows * AnalysisSector.columns;		
		mBodyManager = new BodyManager(this, width, height, numberOfSectors, 1);

	}

	public void draw(){
		mCamera.read();	
		
		clearBackground();
		
		if (mCamera.getPixels() != null){
			
			mCamera.cam.loadPixels();
			
			AnalysisResult analysisData = SectorUtils.analyseSectors(mCamera.cam.pixels);			
			
			AnalysisSector.updateAccumulation(analysisData.found);
			
			displayUpdatedBody(analysisData);			
			SectorUtils.drawAllSectors(randomColor1, randomColor2, this);
		}
	}
	
	private void displayUpdatedBody(AnalysisResult analysisData) {
		
		if (analysisData.foundCount > 20){
			mBodyManager.updateBodies(analysisData.movingPoints);			
		}
		
		mBodyManager.display();
	}
	
	private void clearBackground(){
		if (PLAIN_BACKGROUND){
			noStroke();
			fill(backgroundColor);
			rect(width/2, height/2, width, height);
		}
	}
}
