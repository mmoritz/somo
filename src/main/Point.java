package main;


public class Point {

	public double x,y;

	Point(double x, double y){
		this.x = x;
		this.y = y;
	}

	Point(){
		this.x = 0;
		this.y = 0;
	}
	
	public String toString(){
		return ("Point: " + this.x + ", " + this.y);
	}
}
