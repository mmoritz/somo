package main;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import body.BodyManager;
import processing.core.PApplet;
import processing.core.PConstants;
import processing.video.Capture;
import sectors.AnalysisResult;
import sectors.AnalysisSector;
import sectors.SectorBuilder;
import sectors.SectorUtils;
import osc.OscCom;
//import osc.Ableton;
import oscP5.*;
import processing.core.PGraphics;

@SuppressWarnings("serial")
public class SoMo extends PApplet {
	
	//Ableton oscAbleton;
	
	OscCom OSC_SectorsAndPolygons;
	
	SoMoConfig config;
	int randomColor1, randomColor2;
		
	BodyManager bodyManager;
	AnalysisResult analysisData;
	
	//int [] screenBuf;	//	Job Leonard's blur filter stuff
	
	// Counter to verify if the camera has blocked for more than ~0.5 s (frameRate/2)
	int blocked = 0;
	int lastToken = -1;
	
	public void setup()
	{		
		//oscAbleton = new osc.Ableton(this);
		
		try {
			config = new SoMoConfigBuilder()
					.readConfigurationFile("/config.json")
					.build(this);
		} catch (Exception e) {
			e.printStackTrace();
			println("");
			CameraCapture.logAllCameras(Capture.list());
		}
		
		//((javax.swing.JFrame) frame).getContentPane().setBackground(Color.BLACK);
		
		OSC_SectorsAndPolygons = new OscCom(this, config.oscSectorsPolysReceiver_IpAddress, config.oscSectorsPolysReceiver_Port, config.sectorsCount*2);
		
		println("config.sectorsCount = " + config.sectorsCount);
		
		noCursor();
		frameRate(100);
	
		bodyManager = new BodyManager(config);
		
		rectMode(CENTER);
		colorMode(config.colorMode, 255, 255, 255, 255);
		
		updateRandomColors(bodyManager.getShowBodyPolygons());
		
		//screenBuf = new int[width*height];	//	Job Leonard's blur filter stuff
		/////////////loadPixels();
	}

	public void drawRaw(){
		
		if (config.camera.read()){
			
			background(0);
			
			if (config.camera.getPixels() != null){
				
				config.camera.cam.loadPixels();
				
				analysisData = SectorUtils.analyseSectors(
						config.camera.cam.pixels, config.pixelChangeThreshold);

				AnalysisSector.updateAccumulation(analysisData.found, config.maxSectorAccumulation, config.sectorAccumulationFraction);
				
				config.pd.update(bodyManager, config.screenSize, analysisData.foundCount);
				config.pd.send();
					
				OSC_SectorsAndPolygons.sendChunk(AnalysisSector.states, "/sectorsON_OFF");
				OSC_SectorsAndPolygons.sendChunk(AnalysisSector.accumulations, "/sectorsAccum");
				
				List<Object> msg = new ArrayList<Object>();
				msg.add(bodyManager.mBodies.size());
				for(int b=0; b<bodyManager.mBodies.size(); b++) {
					msg.add(bodyManager.mBodies.get(b).getBodyPolygon().count());
					for(int p=0; p<bodyManager.mBodies.get(b).getBodyPolygon().count(); p++) {
						msg.add(bodyManager.mBodies.get(b).getBodyPolygon().getCoords()[p][0]);
						msg.add(bodyManager.mBodies.get(b).getBodyPolygon().getCoords()[p][1]);
					}
				}
				OSC_SectorsAndPolygons.send(msg, "/polygons");
				
				
				//changeRandomColors();
				SectorUtils.drawAllSectors(randomColor1, randomColor2, this);
				
				//filter( BLUR, 2.0f );	//	Processing built-in blur filter is toooooooooooo slow
				//	This blur is ABSURDLY faster:
				loadPixels();
				  	//post_processing.BlurFilters.shiftBlur2(pixels, screenBuf, width, height);
				  	//post_processing.BlurFilters.shiftBlur2(screenBuf, pixels, width, height);
				  	post_processing.BlurFilters.klingemannBlur(pixels, 5, width, height);
				updatePixels();
			}
			
			blocked = 0;
			
		} else {			
			handleBlockedCamera();
		}
	}
	
	public void draw(){
		
		drawRaw();
		
		if(analysisData != null && bodyManager.getShowBodyPolygons()) {
			displayUpdatedBody(analysisData);
		}
		
		//config.watcherOSC.send(100.f/frameRate, config.oscWatcherMsgName_100_over_FPS);
		
		//text(frameRate,50,50);
		
		if(mousePressed)
		{
			int as2;
			for (int as=0; as < (AnalysisSector.sectors.length); as++)
			{
				as2 = as;
				if (AnalysisSector.mirror){
					as2 = (AnalysisSector.columns -1 -(as % AnalysisSector.columns) 
							+ (AnalysisSector.columns)*((int)(as/AnalysisSector.columns)));
				}
				//	This is for manually masking problematic pixels that you can't avoid in a stage situation 
				if(AnalysisSector.sectors[as2].IsInsideSector(mouseX, mouseY))
				{
					System.out.println("as = " + as);
					System.out.println("as2 = " + as2);
					System.out.println("AnalysisSector.columns = " + AnalysisSector.columns);
					AnalysisSector.sectors[as].setIsON(mouseButton==RIGHT);
				}
			}
		}
	}
	
	private void handleBlockedCamera() {
		blocked++;
		
		if (blocked > frameRate/2){
			config.camera.informFailure();
			blocked = 0;
			if (lastToken == LEFT){
				this.config.camera.previousCamera();
			} else if (lastToken == RIGHT){
				this.config.camera.nextCamera();
			} else if (lastToken == UP || lastToken == DOWN){
				this.config.camera.retryResolution();
			} else{
				this.config.camera.nextCamera();
			}
							
			resetScreenSizeAndSectors();
		}
	}
	
//	void oscEvent(OscMessage theOscMessage) {
//		oscAbleton.oscEvent(theOscMessage);
//	}
	
	///	Many manual controls via keyboard, to be sent to this application, to the synth (OSC, local) and to the watcher (OSC)  
	public void keyReleased()
	{
//		oscAbleton.test();
//		oscAbleton.getDeviceName();
//		oscAbleton.getParamRange();
//		oscAbleton.getTrackName();
		
		if (key == CODED)
		{
			switch(keyCode)
			{
				case LEFT:
					this.config.camera.previousCamera();
					resetScreenSizeAndSectors();
					lastToken = LEFT;
				break;
				case RIGHT:
					this.config.camera.nextCamera();				
					resetScreenSizeAndSectors();
					lastToken = RIGHT;
				break;
				case UP:
					this.config.camera.nextCameraResolution();
					resetScreenSizeAndSectors();
					lastToken = UP;
				break;
				case DOWN:
					this.config.camera.previousCameraResolution();
					resetScreenSizeAndSectors();
					lastToken = DOWN;
				break;
			} 
		}
		else {
			List<Object> data = new ArrayList<Object>();
			switch(keyCode)
			{
				case java.awt.event.KeyEvent.VK_1:
					noCursor();
				break;
				case java.awt.event.KeyEvent.VK_2:
					cursor();
				break;
				case java.awt.event.KeyEvent.VK_P:	//	polygons on/off
					bodyManager.setShowBodyPolygons(!bodyManager.getShowBodyPolygons());
				break;
				case java.awt.event.KeyEvent.VK_C:	//	change colors
					updateRandomColors(bodyManager.getShowBodyPolygons());
				break;
				case java.awt.event.KeyEvent.VK_A:	//	accum fraction --
					config.sectorAccumulationFraction--;
					System.out.println("config.sectorAccumulationFraction = " + config.sectorAccumulationFraction);
					if(config.sectorAccumulationFraction == 0) {
						config.sectorAccumulationFraction = 1;
					}
					config.watcherOSC.send(config.sectorAccumulationFraction/30.f, config.oscWatcherMsgName_AccFraction_over_30);
				break;
				case java.awt.event.KeyEvent.VK_S:	//	accum fraction ++
					config.sectorAccumulationFraction++;
					System.out.println("config.sectorAccumulationFraction = " + config.sectorAccumulationFraction);
					config.watcherOSC.send(config.sectorAccumulationFraction/30.f, config.oscWatcherMsgName_AccFraction_over_30);
				break;
				case java.awt.event.KeyEvent.VK_SUBTRACT:	//	threshold --
					config.pixelChangeThreshold -= 1.5;
					System.out.println("config.pixelChangeThreshold = " + config.pixelChangeThreshold);
					config.watcherOSC.send(config.pixelChangeThreshold/100.f, config.oscWatcherMsgName_PixChangeThreshold_over_100);
				break;
				case java.awt.event.KeyEvent.VK_ADD:	//	threshold ++
					config.pixelChangeThreshold += 1.5;
					System.out.println("config.pixelChangeThreshold = " + config.pixelChangeThreshold);
					config.watcherOSC.send(config.pixelChangeThreshold/100.f, config.oscWatcherMsgName_PixChangeThreshold_over_100);
				break;
				case java.awt.event.KeyEvent.VK_D:	//	Distorter
					if(config.pd.clipperON == 0) config.pd.clipperON = 1;
					else                         config.pd.clipperON = 0;
					config.pd.send_manual();
					System.out.println("config.pd.clipperON = " + config.pd.clipperON);
					config.watcherOSC.send(config.pd.clipperON, config.oscWatcherMsgName_Distorter);
				break;
				case java.awt.event.KeyEvent.VK_U:	//	Uivo
					if(config.pd.uivoON == 0) config.pd.uivoON = 1;
					else                      config.pd.uivoON = 0;
					config.pd.send_manual();
					System.out.println("config.pd.uivoON = " + config.pd.uivoON);
					config.watcherOSC.send(config.pd.uivoON, config.oscWatcherMsgName_Uivo);
				break;
				case java.awt.event.KeyEvent.VK_N:	//	levelMultiplier-
					config.pd.levelMultiplier -= 0.1f;
					config.pd.send_manual();
					System.out.println("config.pd.levelMultiplier = " + config.pd.levelMultiplier);
					config.watcherOSC.send(config.pd.levelMultiplier, config.oscWatcherMsgName_LevelMultiplier);
				break;
				case java.awt.event.KeyEvent.VK_M:	//	levelMultiplier+
					config.pd.levelMultiplier += 0.1f;
					config.pd.send_manual();
					System.out.println("config.pd.levelMultiplier = " + config.pd.levelMultiplier);
					config.watcherOSC.send(config.pd.levelMultiplier, config.oscWatcherMsgName_LevelMultiplier);
				break;
				case java.awt.event.KeyEvent.VK_0:
					config.pd.levelMultiplier = 0.0f;
					config.pd.send_manual();
					System.out.println("config.pd.levelMultiplier = " + config.pd.levelMultiplier);
					config.watcherOSC.send(config.pd.levelMultiplier, config.oscWatcherMsgName_LevelMultiplier);
				break;
				case java.awt.event.KeyEvent.VK_9:
					config.pd.levelMultiplier = 1.0f;
					config.pd.send_manual();
					System.out.println("config.pd.levelMultiplier = " + config.pd.levelMultiplier);
					config.watcherOSC.send(config.pd.levelMultiplier, config.oscWatcherMsgName_LevelMultiplier);
				break;
			}
		}
		println(frameRate);
	}
	
	private void resetScreenSizeAndSectors(){

		config.updateScreenSize();
		
		SectorBuilder.createSectors(config);
		
		bodyManager.setScreenSize(height, width);
	}
	
	private void displayUpdatedBody(AnalysisResult analysisData) {
		
		if (analysisData.foundCount > 3){
			bodyManager.updateBodiesWithJavaML(analysisData.movingPoints);
		}
		
		bodyManager.display();
	}
	
	//private void changeRandomColors() {
	//	if (frameCount % 400/*6*frameRate*/ == 0){
	//		updateRandomColors();
	//	}
	//}
	
	void updateRandomColors(boolean showBodyPolygons)
	{	
		//		Avoids red hues because the body polygon is red
		if(showBodyPolygons)
		{
			int hue = (int)random(33, 255);
			float alpha = /*210 + random(45));*/100 + random(155);
			processing.core.PApplet.println("randomColor1: hue = " + hue + " | alpha = " + alpha);
			randomColor1 = color(hue, 255, 255, alpha);
			
			hue = (int)random(33, 255);
			alpha = /*70 + random(50));*/random(90);
			processing.core.PApplet.println("randomColor2: hue = " + hue + " | alpha = " + alpha);
			randomColor2 = color(hue, 255, 255, alpha);
		}
		else	//	Avoids blue hues because they don't offer contrast with black background. 
		{	
			float opt = random(1);
			int hue;
			/*if(opt < 0.5f) {
				hue = (int)random(32, 130);		
			} else {
				hue = (int)random(208,250);
			}*/
			hue = (int)random(255);
			
			float alpha = /*random(175);*/100 + random(155);
			processing.core.PApplet.println("randomColor1: hue = " + hue + " | alpha = " + alpha);
			
			randomColor1 = color(hue, 255, 255, alpha);
			
			opt = random(1);
			/*if(opt < 0.5f) {
				hue = (int)random(32, 130);		
			} else {
				hue = (int)random(208,250);
			}*/
			hue = (int)random(255);
			
			alpha = 50 + random(50);//random(90);
			processing.core.PApplet.println("randomColor2: hue = " + hue + " | alpha = " + alpha);
			
			randomColor2 = color(hue, 255, 255, alpha);
		}
	}

}
