package main;
import java.util.ArrayList;
//import java.util.Random;

import processing.core.PVector;


public final class VideoUtils {
	
	/**
	 * Prevent the class to be instantiated.
	 */
	private VideoUtils(){}
	
	
	
	/** K-Means Algorithm to divide the data into clusters.
	 * 
	 * @param data 			: float[n][2] are the points to be divided in clusters
	 * @param maxClusters 	: maximum number of clusters to fit the data
	 * @return 				: ArrayList<float[][]> with the cluster points
	 */
	public static ArrayList<float[][]> getKMeansClusters(float[][] data, int maxClusters){
		
//		int numberOfRuns 	= 5;
		
		// Array to store data of a given K to select the best try
//		ArrayList<ArrayList<Object>> tempKMeans;
		
//		int bestTmpKMeans = 0;
		
		// Array to store the final results of KMeans clustering.
		ArrayList<ArrayList<Object>> kMeansData = new ArrayList<ArrayList<Object>>();
		
		
		int useK = 1;
		double sqrErrorToUseK = -1;
//		double sqrErrorToUseIterate = -1;
		
		// Define the number of clusters running with 1 to 5
		for (int k=1; k<maxClusters+1; k++){
			
			
			// This appear to be very expensive to be done in real time. FIXME: not working
//			tempKMeans = new ArrayList<ArrayList<Object>>();
//			
//			for (int i=0; i<numberOfRuns;i++){
//				tempKMeans.add(runKMeans(data, i+1));
//			}
//			
//			// TODO: try to put this together with the previous loop
//			for (int i=0; i<numberOfRuns;i++){
//				if (sqrErrorToUseIterate == -1 || (double) tempKMeans.get(i).get(0) < sqrErrorToUseIterate){
//					bestTmpKMeans = i;
//					sqrErrorToUseIterate = (double) tempKMeans.get(i).get(0);
//				}
//			}
//			
//			kMeansData.add(tempKMeans.get(bestTmpKMeans));
//			sqrErrorToUseIterate = -1;
//			tempKMeans.clear();
			
			kMeansData.add(runKMeans(data, k));
			
//			System.out.println("K-Means error with K="+i+": " + (double) kMeansData.get(i-1).get(0)
//					+ " looped " + (int) kMeansData.get(i-1).get(4) + " times.");
			
			// Decide which K to use
			if (sqrErrorToUseK == -1 || (double) kMeansData.get(k-1).get(0) < .3 * sqrErrorToUseK){ //FIXME: hardcoded: 0.1
				useK = k;
				sqrErrorToUseK = (double) kMeansData.get(k-1).get(0);
			}
		}
		
//		System.out.println("use k = " + useK);

		ArrayList<float[][]> clusters = new ArrayList<float[][]>();
		
		clusters = getClusters(data, useK, kMeansData.get(useK-1));
		
//		for (int i=0; i<clusters.size();i++){
//			System.out.println("Cluster " + i + " length: " + clusters.get(i).length);
//		}
		
		
		return clusters;
	}
	
	public static ArrayList<Object> runKMeans(float[][] data, int k){
		
		PVector[] centroids = new PVector[k]; 
//		Random random = new Random();
		double sqrError = 999999999.;
		
		
		// Randomly initialize K cluster centroids mi[k];
		for (int i=0; i<k; i++){
			
			// FIXME: hardcoded
			
			// randomly distributed
//			centroids[i] = new PVector(random.nextFloat() * 640, 
//					random.nextFloat() * 480);
			
			// horizontally distributed
			centroids[i] = new PVector(640/k/2+ i*640/k, 240);
		}

		ArrayList<Object> kMeansData = runKMeansIteration(data, k, centroids);
		int countLoops = 1;
		
		while ((double) kMeansData.get(0) < sqrError){
			sqrError = (double) kMeansData.get(0);
			kMeansData = runKMeansIteration(data, k, (PVector[]) kMeansData.get(1));
			countLoops++;
		}
		
		kMeansData.add(countLoops);
		
		return kMeansData;
	}

	/**
	 * I must run this algorithm a couple times to make sure I didn't stuck with
	 * two points together.
	 * @param data	: data points
	 * @param k		: number of clusters
	 * @return		: to be defined
	 */
	public static ArrayList<Object> runKMeansIteration
	(float[][] data, int k, PVector[] currentPosition){
		
		ArrayList<Object> result = new ArrayList<Object>();
		
		PVector[] centroid = new PVector[k];
		int [] countClusterPoints = new int [k];
		
		for (int i=0; i<k; i++){
			// Initialize centroid positions
			centroid[i] = new PVector(0, 0);
	
			// Initialize cluster points counters
			countClusterPoints[i] = 0;
		}
		
		int [] closestClusterIndex = new int[data.length];
	
		
		double sqrError = 0.;
		
		
		// Assing a cluster to each point
		for (int i=0; i<data.length;i++){
			closestClusterIndex[i] = 0;
			double smallerSqrDistance = 99999999999.;
			for (int j=0;j<k;j++){
			
//				System.out.println("debug currPos[j].x: " + currentPosition[j].x);
//				System.out.println("debug currPos[j].y: " + currentPosition[j].y);
//				System.out.println("debug data[i][0]: " + data[i][0]);
//				System.out.println("debug data[i][1]: " + data[i][1]);

				double xDiff = data[i][0] - currentPosition[j].x;
				double yDiff = data[i][1] - currentPosition[j].y;
				
				double distance = Math.pow(xDiff, 2.) + Math.pow(yDiff, 2.);
				
//				System.out.println("distance: " + distance);
				
				if (distance < smallerSqrDistance){
					smallerSqrDistance = distance;
					closestClusterIndex[i] = j;
//					System.out.println("smallerSqrDistance: " + smallerSqrDistance);
				} else {
//					System.out.println("smallerSqrDistance: " + smallerSqrDistance);
					
//					if (smallerSqrDistance > 88888888.){
//						System.out.println("debug currPos[j].x: " + currentPosition[j].x);
//						System.out.println("debug currPos[j].y: " + currentPosition[j].y);
//						System.out.println("debug data[i][0]: " + data[i][0]);
//						System.out.println("debug data[i][1]: " + data[i][1]);
//					}
				}
			}
			sqrError += smallerSqrDistance;
			countClusterPoints[closestClusterIndex[i]]++;
		}
		sqrError /= data.length;
		
		
		// Update the centroids of each cluster		
		
		// need this second loop over data to get the centroids
		for (int i=0;i<data.length;i++){
			centroid[closestClusterIndex[i]].x += data[i][0];
			centroid[closestClusterIndex[i]].y += data[i][1];
		}
		
		// need this second loop over clusters to get the centroids
		for (int i = 0; i<k;i++){
			if (countClusterPoints[i] > 0){
				centroid[i].x /= countClusterPoints[i];
				centroid[i].y /= countClusterPoints[i];
			}
		}
		
		result.add(sqrError);
		result.add(centroid);
		result.add(closestClusterIndex);
		result.add(countClusterPoints);
		
//		printKMeansData(result);
		
		return result;
	}
	
	public static ArrayList<float[][]> getClusters(float[][] data, int k, ArrayList<Object> kMeansData){
		
//		printKMeansData(kMeansData);
		
		ArrayList<float[][]> clusters = new ArrayList<float[][]>();
//		ArrayList<PVector> points = new ArrayList<PVector>();
		int [] countClusterPoints = (int[]) kMeansData.get(3);
				
		// cluster index
		int [] closestClusterIndex = (int[]) kMeansData.get(2);
		
		for (int i=0; i<k; i++){
			
			float[][] points = new float [countClusterPoints[i]][2];
			
			if (countClusterPoints[i] > 0){
	//			System.out.println("countClusterPoints["+i+"]: "+countClusterPoints[i]);
				int index = 0;
				for (int j=0;j<data.length;j++){
					if (closestClusterIndex[j] == i){
						points[index][0] = data[j][0];
						points[index][1] = data[j][1];
						index++;
					}
				}
			}
			clusters.add(points);
		}
		
		return clusters;
	}
	
	public static void printKMeansData(ArrayList<Object> kMeansData){
		
		int k 			= 	((PVector[]) kMeansData.get(1)).length;
		int dataLength 	= 	((int[])		kMeansData.get(2)).length;
		
		System.out.println("kMeans.get(0) - sqrError:  		" + (double)	kMeansData.get(0));
		for (int i=0;i<k;i++){
			System.out.println("kMeans.get(1) - centroids[" + i +"]: "+ ((PVector[]) kMeansData.get(1))[i].x
			+ ", " + ((PVector[])	kMeansData.get(1))[i].y);
			
			System.out.println("kMeans.get(3) - clusterPoints[" + i +"]: " + ((int[])	kMeansData.get(3))[i]);
		}
		
		for (int i=0;i<dataLength;i++){
			System.out.println("kMeans.get(2) - clusterIndex[" + i +"]: " + ((int[]) kMeansData.get(2))[i]);
		}
		
//		System.out.println("kMeans.get(4) - n Loops: 		" + (int)		kMeansData.get(4));
	}
}
