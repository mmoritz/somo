package main;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
* Maps the resolutions to integer constants. The resolutions are based on
* https://en.wikipedia.org/wiki/Graphics_display_resolution
*/
class VideoResolutions {

    /**
	 * QQVGA (160x120)
	 */
	final public static int QQVGA 			= -1;

	/**
	 * HQVGA (240x160)
	 */
	final public static int HQVGA 			= -2;

	/**
	 * QVGA (320x240)
	 */
	final public static int QVGA 			= -3;


	/**
	 * WQVGA (400x240)
	 */
	final public static int WQVGA 			= -4;

	/**
	 * HVGA (480x320)
	 */
	final public static int HVGA 			= -5;

	/**
	 * nHD (640x360)
	 */
	final public static int nHD 			= -6;

	/**
	 * VGA resp. SD (640x480)
	 */
	final public static int VGA 			= -7;

	/**
	 * WVGA (768x480)
	 */
	final public static int WVGA 			= -8;

	/**
	 * FWVGA (854x480)
	 */
	final public static int FWVGA 			= -9;

	/**
	 * SVGA (800x600)
	 */
	final public static int SVGA 			= -10;

	/**
	 * qHD (960x540)
	 */
	final public static int qHD 			= -11;

	/**
	 * DVGA (960x640)
	 */
	final public static int DVGA 			= -12;

	/**
	 * WSVGA (1024x576/600)
	 */
	final public static int WSVGA 			= -13;

	/**
	 * XGA (1024x768)
	 */
	final public static int XGA 			= -14;

	/**
	 * HD (1280x720)
	 */
	final public static int HD 				= -15;

	/**
	 * WXGA_1 1366x768
	 */
	final public static int WXGA_1 			= -16;

	/**
	 * WXGA_2 1360x768
	 */
	final public static int WXGA_2 			= -17;

	/**
	 * WXGA_3 1280x800
	 */
	final public static int WXGA_3 			= -18;

	/**
	 * XGA+ (1152x864)
	 */
	final public static int XGA_PLUS 		= -19;

	/**
	 * SXGA (1280x1024)
	 */
	final public static int SXGA 			= -20;

	/**
	 * WXGA+ (1440x900)
	 */
	final public static int WXGA_PLUS 		= -21;

	/**
	 * SXGA+ (1400x1050)
	 */
	final public static int SXGA_PLUS 		= -22;

	/**
	 * WSXGA+ (1680x1050)
	 */
	final public static int WSXGA_PLUS 		= -23;
	
	/**
	 * UXGA (1600x1200)
	 */
	final public static int UXGA 			= -24;

	/**
	 * FHD (1920x1080)
	 */
	final public static int FHD 			= -25;
	
	/**
	 * WUXGA (1920x1200)
	 */
	final public static int WUXGA 			= -26;

	/**
	 * QWXGA (2048x1152)
	 */
	final public static int QWXGA 			= -27;

	/**
	 * QXGA (2048x1536)
	 */
	final public static int QXGA 			= -28;

	/**
	 * QHD (2560x1440)
	 */
	final public static int QHD 			= -29;

	/**
	 * WQXGA (2560x1600)
	 */
	final public static int WQXGA 			= -30;

	/**
	 * QSXGA (2560x2048)
	 */
	final public static int QSXGA 			= -31;

	/**
	 * WQXGA+ (3200x1800)
	 */
	final public static int WQXGA_PLUS 		= -32;

	/**
	 * WQSXGA (3200x2048)
	 */
	final public static int WQSXGA 			= -33;	

	/**
	 * QUXGA (3200x2400)
	 */
	final public static int QUXGA 			= -34;	

	/**
	 * UHD 4K (3840x2160)
	 */
	final public static int UHD 			= -35;

	/**
	 * Digital Cinema Initiatives 4K (4096x2160)
	 */
	final public static int DCI_4K 			= -36;

	/**
	 * WQUXGA (3840x2400)
	 */
	final public static int WQUXGA 			= -37;

	/**
	 * HXGA (4096x3072)
	 */
	final public static int HXGA 			= -38;

	/**
	 * UHD+ 5K (5120x2880)
	 */
	final public static int UHD_PLUS 		= -39;

	/**
	 * WHXGA (5120x3200)
	 */
	final public static int WHXGA 			= -40;

	/**
	 * HSXGA (5120x4096)
	 */
	final public static int HSXGA 			= -41;

	/**
	 * WHSXGA (6400x4096)
	 */
	final public static int WHSXGA 			= -42;

	/**
	 * HUXGA (6400x4800)
	 */
	final public static int HUXGA 			= -43;

	/**
	 * FUHD 8K (7680x4320)
	 */
	final public static int FUHD 			= -44;

	/**
	 * WHUXGA (7680x4800)
	 */
	final public static int WHUXGA 			= -45;

	/**
	 * QUHD 16K (15360x8640)
	 */
	final public static int QUHD 			= -46;

	/**
	 * Maps the integer codes resolution index to their respective resolution text:
	 * {VGA, "640x480"}.
	 */
	final private static Map<Integer, String> resolutionMap = createResolutionMap();

	public static Map<Integer, String> getResolutionMap(){
		return resolutionMap;
	}
	
	/**
	 * Maps the integer codes resolultion index to their respective resolution text:
	 * {VGA, "640x480"}.
	 */
	public static Map<Integer, String> createResolutionMap(){
		
		Map<Integer, String> resolution = new HashMap<Integer, String>();

		// Video Graphics Array
		resolution.put(QQVGA 	,	"160x120");
		resolution.put(HQVGA 	,	"240x160");
		resolution.put(QVGA 	,	"320x240");
		resolution.put(WQVGA 	,	"400x240");
		resolution.put(HVGA 	,	"480x320");
		resolution.put(VGA		,	"640x480");
		resolution.put(WVGA 	,	"768x480");
		resolution.put(FWVGA 	,	"854x480");
		resolution.put(SVGA 	,	"800x600");
		resolution.put(DVGA 	,	"960x640");
		resolution.put(WSVGA 	,	"1024x576");
		


		// Extended Graphics Array
		resolution.put(XGA 			,	"1024x768");
		resolution.put(WXGA_1 		,	"1366x768");
		resolution.put(WXGA_2 		,	"1360x768");
		resolution.put(WXGA_3 		,	"1280x800");
		resolution.put(XGA_PLUS 	,	"1152x864");
		resolution.put(WXGA_PLUS	,	"1440x900");
		resolution.put(SXGA 		,	"1280x1024");
		resolution.put(SXGA_PLUS 	,	"1400x1050");
		resolution.put(WSXGA_PLUS 	,	"1680x1050");
		resolution.put(UXGA 		,	"1600x1200");
		resolution.put(WUXGA 		,	"1920x1200");



		// Quad Extended Graphics Array
		resolution.put(QWXGA		,	"2048x1152");
		resolution.put(QXGA			,	"2048x1536");
		resolution.put(WQXGA		,	"2560x1600");
		resolution.put(QSXGA		,	"2560x2048");
		resolution.put(WQSXGA		,	"3200x2048");
		resolution.put(QUXGA		,	"3200x2400");
		resolution.put(WQUXGA		,	"3840x2400");



		// Hyper Extended Graphics Array
		resolution.put(HXGA 		,	"4096x3072");
		resolution.put(WHXGA 		,	"5120x3200");
		resolution.put(HSXGA 		,	"5120x4096");
		resolution.put(WHSXGA		,	"6400x4096");
		resolution.put(HUXGA 		,	"6400x4800");
		resolution.put(WHUXGA		,	"7680x4800");

		// High-Definition
		resolution.put(nHD 			,	"640x360");
		resolution.put(qHD 			,	"960x540");
		resolution.put(HD  			,	"1280x720");
		resolution.put(FHD 	 		,	"1920x1080");
		resolution.put(QHD 	 		,	"2560x1440");
		resolution.put(WQXGA_PLUS	,	"3200x1800");
		resolution.put(UHD 	 		,	"3840x2160");
		resolution.put(DCI_4K	 	,	"4096x2160");
		resolution.put(UHD_PLUS  	,	"5120x2880");
		resolution.put(FUHD 	 	,	"7680x4320");
		resolution.put(QUHD		 	,	"15360x8640");

		return resolution;
	}
	
	/**
	 * TODO NOT TESTED! Get the selected camera's height and the width in 
	 * pixels and returns it in an array.
	 */
	public static int[] getScreenSize(int resolution) {
		String resolutionText = resolutionMap.get(resolution);  
		
		int[] result = new int[2];
		int indexOf_x_ = resolutionText.indexOf('x');

		result[0] = Integer.parseInt(resolutionText.substring(0, indexOf_x_));
		result[1] = Integer.parseInt(resolutionText.substring(indexOf_x_ + 1));

		return result;
	}
	
	/**
	 * Gives the index of the highest resolution screen based
	 * on the product width x height.
	 * @return the index of the highest resolution screen
	 */
	public static int getHighestResolutionIndex(){
		
		// resolution number: width x height
		long largestResolutionProduct = 0;
		int index = 0;
		
		Iterator<Map.Entry<Integer,String>> it = resolutionMap.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<Integer, String> pair = it.next();
	        int key = pair.getKey();
	        
	        // from key, get the resolution
	        int[] resolution = getScreenSize(key);
	        long resolutionProduct = resolution[0] *  resolution[1];
	        
	        if (resolutionProduct > largestResolutionProduct){
	        	index = key;
	        	largestResolutionProduct = resolutionProduct;
	        }
	        
//	        it.remove(); // avoids a ConcurrentModificationException
	    }
	    
	    return index;
	}
	
	/**
	 * Gives the index of the lowest resolution screen based
	 * on the product width x height.
	 * @return the index of the lowest resolution screen
	 */
	public static int getLowestResolutionIndex(){
		
		// resolution number: width x height
		long smallestResolutionProduct = 0;
		int index = 0;
		
		Iterator<Map.Entry<Integer,String>> it = resolutionMap.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<Integer, String> pair = it.next();
	        int key = pair.getKey();
	        
	        // from key, get the resolution
	        int[] resolution = getScreenSize(key);
	        long resolutionProduct = resolution[0] *  resolution[1];
	        
	        if (resolutionProduct < smallestResolutionProduct || smallestResolutionProduct == 0){
	        	index = key;
	        	smallestResolutionProduct = resolutionProduct;
	        }
	    }
	    return index;
	}
	
	/**
	 * Prints on Console the list of resolution constants in this file. 
	 */
	public static void logAllResolutions(){
		Iterator<Map.Entry<Integer,String>> it = resolutionMap.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<Integer, String> pair = it.next();
	        System.out.println(pair.getKey() + " = " + pair.getValue());
	    }
	}
}