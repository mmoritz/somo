package main;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.io.*;

import processing.core.PApplet;
import processing.core.PImage;
import processing.video.*;

import sectors.Point;

/**
 * 
 * @author Moritz/Brizolara;
 * 
 *         Class that creates an instance of the Capture class and handles the
 *         camera configurations and data. It has the capability of dealing 
 *         with cameras on your system list that doesn't work and store its 
 *         records in a file. It also allows you to navigate throughout 
 *         standard resolutions (see class VideoResolutions) and throughout 
 *         your system cameras with simple method calls.
 *         
 *         
 *         To avoid problems using stored  data in blacklist, follow these 
 *         steps:
 *         	- Set the 3rd. parameter of your constructor with the flag 
 *         BLACKLIST_RESET, to use only the data from this session and clean 
 *         up history.
 *          - Run the program and navigate throughout all the resolutions and 
 *          all cameras. One complete turn will speed up the following turns.
 *          - Stop the program, copy the files saved on "../bin/data/" named 
 *          "blacklist" and "cameras" to the folder "../src/data/".
 *          - Set your constructor with the flag BLACKLIST_BLOCK_UPDATES.
 *          - Go to Project->Clean.
 *          You must repeat the procedure if you change your OS or video 
 *          hardware.
 *         
 *
 */
class CameraCapture {
	
	/**
	 * Clear the existing blacklist and use a clean sheet. 
	 */
	public static final int BLACKLIST_RESET = 100;
	
	/**
	 * Use the stored data in blacklist and allow updates.
	 */
	public static final int BLACKLIST_ALLOW_UPDATES  = 101;
	
	/**
	 * Use the stored data in blacklist and block updates.
	 */
	public static final int BLACKLIST_BLOCK_UPDATES  = 102;
	
	/**
	 * Ignores the existence of a blacklist. 
	 */
	public static final int BLACKLIST_IGNORE = 103;
	
	/**
	 * Directory storage of cached data files. The files will be taken from
	 * "/src/CACHE_DIR" in the first run or after a "Clean Project" and then
	 * will be stored in "/bin/CACHE_DIR". Your good version may be stored at
	 * "/src/CACHE_DIR" to allow you to rollback to this version Cleaning up
	 * your project. To use your BLACKLIST_FILENAME file you will need to store
	 * also a LIST_OF_CAMERAS_FILENAME file with data that matches your system.   
	 */
	private final String CACHE_DIR = "data";
	
	/**
	 * Filename to store blacklist data persistently.
	 */
	private final String BLACKLIST_FILENAME = "blacklist";
	
	/**
	 * Store the list of cameras to check if it has changed. If so, ignore 
	 * blacklist. 
	 */
	private final String LIST_OF_CAMERAS_FILENAME = "cameras";

	/**
	 * Maps the integer codes for resolution index to its respective resolution 
	 * text (e.g.: VGA -> "640x480").
	 */
	Map<Integer, String> resolution = VideoResolutions.getResolutionMap();

	/**
	 * Data type for storing and manipulating video frames from an attached
	 * capture device such as a camera. Use Capture.list() to show the names of
	 * any attached devices. Using the version of the constructor without name
	 * will attempt to use the last device used by a QuickTime program.
	 */
	public Capture cam;

	/**
	 * Instance of Processing Applet (PApplet)
	 */
	PApplet processing;

	/**
	 * Index of the selected camera in the list of available cameras.
	 */
	int selectedCameraIndex = -1;

	/**
	 * When the selection is made by resolution, it stores the selected
	 * resolution.
	 */
	public int selectedResolution = 0;

	/**
	 * Camera indexes that were tested and failed miserably.
	 */
	List<Integer> indexesBlackList = new ArrayList<>();
	
	/**
	 * Maked true if the last change in resolution were to a greater 
	 * resolution. False otherwise. 
	 */
	boolean up = false;
	
	/**
	 * Selects among behaviors relative to data stored. The standard value is
	 * BLACKLIST_RESET, which starts with a clean blacklist every restart.
	 * 	- BLACKLIST_RESET
	 *  - BLACKLIST_ALLOW_UPDATES
	 *  - BLACKLIST_BLOCK_UPDATES
	 *  - BLACKLIST_IGNORE
	 */
	int blacklistSettings = BLACKLIST_RESET;
	
	/**
	 * Stores locally the list of available cameras 
	 */
	private final String[] systemCams = Capture.list();
	
	/**
	 * Constructor that creates a instance of the Capture.
	 * 
	 * @param processing
	 *            instance of PApplet
	 * @param selectedCameraIndex
	 *            the index of selected camera in the list given by the method
	 *            Capture.list() and stored in systemCams. Passing resolution 
	 *            constant from VideoResolutions will create a request to attempt
	 *            to use this resolution.
	 * @param blacklistSettings
	 * 			  selects what to do when find problems with selections. Allows 
	 * 			  you to store the failed requests and avoid repeating it.  
	 */
	CameraCapture(PApplet processing, int selectedCameraIndex, int blacklistSettings) {
		this.blacklistSettings = blacklistSettings;
		commonConstructor(processing, selectedCameraIndex);
	}
	
	/**
	 * Constructor that creates a instance of the Capture.
	 * 
	 * @param processing
	 *            instance of PApplet
	 * @param selectedCameraIndex
	 *            the index of selected camera in the list given by the method
	 *            Capture.list() and stored in systemCams. Passing resolution 
	 *            constant from VideoResolutions will create a request to attempt
	 *            to use this resolution.
	 */
	CameraCapture(PApplet processing, int selectedCameraIndex) {
		commonConstructor(processing, selectedCameraIndex);
	}
	
	private void commonConstructor(PApplet processing, int selectedCameraIndex){
		this.processing = processing;
		loadBlacklist();
		setCamera(selectedCameraIndex);
		logAllCameras(systemCams, indexesBlackList, this.selectedCameraIndex);
	}
	
	private boolean isCameraDataValid(){
		List<String> ls = getListOfStringsFromFile(CACHE_DIR, LIST_OF_CAMERAS_FILENAME);
				
		String[] camerasFromFile = new String[ls.size()];
		ls.toArray(camerasFromFile);
		
		if (camerasFromFile.length == systemCams.length){
			for (int i=0; i<camerasFromFile.length; i++){
				if (!camerasFromFile[i].equals(systemCams[i])){
					storeAnArrayOfStringsToAFile(CACHE_DIR, LIST_OF_CAMERAS_FILENAME, systemCams);
					deleteFile(CACHE_DIR, BLACKLIST_FILENAME);
					return false;
				}
			}
			
		} else{
			storeAnArrayOfStringsToAFile(CACHE_DIR, LIST_OF_CAMERAS_FILENAME, systemCams);
			deleteFile(CACHE_DIR, BLACKLIST_FILENAME);
			return false;
		}
		return true;
	}
	
	/**
	 * Set the current Camera.
	 * 
	 * @param processing
	 * @param selectedCameraIndex - if < 0 it indexes a resolution as defined in VideoResolutions.java,
	 * then selects the fastest device with that resolution. If >= 0 assumes it is the index of the
	 * device, so doesn't make any search, just picks it  
	 */
	public void setCamera(int selectedCameraIndex) {

		if (selectedCameraIndex > -1) {
			this.selectedCameraIndex = selectedCameraIndex;

			if (systemCams.length == 0) {
				System.out.println("There are no cameras available for capture.");
				processing.exit();
			} else {
				
				/**
				 * Tries to assign the selected Capture object to "cam", if it
				 * fails assign the first camera on the list.
				 */
				try {
					if (this.cam != null) {
						this.cam.stop();
					}
					System.out.println("selectedCameraIndex = " + selectedCameraIndex);
					cam = new Capture(processing, systemCams[selectedCameraIndex]);
					cam.start();
				} catch (ArrayIndexOutOfBoundsException e) {
					e.printStackTrace();
				}

				if (cam == null) {
					System.out.println(
							"\n\nSystem failed to load the selected camera, " + "taking " + systemCams[0] + ", instead.");
					cam = new Capture(processing, systemCams[0]);
					cam.start();
				}
			}
		} else {
			// Stores the last resolution selection.
			this.selectedResolution = selectedCameraIndex;
			
			// Try to find a good match.
			this.selectedCameraIndex = matchScreenSize(
					systemCams, 
					resolution.get(selectedCameraIndex));

			// If succeed assign it to the Camera.
			if (this.selectedCameraIndex > -1)
				setCamera(this.selectedCameraIndex);
			else {
				
				if (blacklistSettings != BLACKLIST_IGNORE
						&& blacklistSettings != BLACKLIST_BLOCK_UPDATES)
				{
					addIndexToBlacklist(this.selectedResolution);
				}
					

				if (up)
					nextCameraResolution();
				else 
					previousCameraResolution();
			}
		}
	}

	/**
	 * Checks if the Capture cam is available and if it is, reads the pixels and
	 * returns true. Returns false otherwise.
	 * 
	 * @return success
	 */
	public boolean read() {
		if (cam.available() == true) {
			cam.read();
			return true;
		} else
			return false;
	}

	public PImage toImage() {
		return cam;
	}

	public int[] getPixels() {
		return cam.pixels;
	}

	// *** Display methods ***

	public void displayImage() {
		displayImage(0, 0);
	}

	public void displayImage(int posX, int posY) {
		processing.image(cam, posX, posY);
	}

	public void displayHozirontallyMirroredImage() {
		displayHozirontallyMirroredImage(0, 0);
	}

	public void displayHozirontallyMirroredImage(int posX, int posY) {
		processing.pushMatrix();
		processing.scale(-1, 1);
		processing.image(cam, -processing.width + posX, posY);
		processing.popMatrix();
	}

	// *** switch camera methods ***

	/**
	 * Switch to the next camera on the available cameras list.
	 * 
	 * @param processing
	 *            - PApplet instance
	 */
	public void nextCamera() {
		setCamera(nextIndex(selectedCameraIndex));
	}

	/**
	 * Get the next index in the camera list. If the current index is the last
	 * one, returns 0.
	 * 
	 * @param currentIndex
	 */
	private int nextIndex(int currentIndex) {
		if (indexesBlackList.contains((currentIndex + 1) % systemCams.length))
			return nextIndex(++currentIndex % systemCams.length);
		else
			return (++currentIndex % systemCams.length);
	}

	/**
	 * Switch to the previous camera on the available cameras list.
	 * 
	 * @param processing
	 *            - PApplet instance
	 */
	public void previousCamera() {
		setCamera(previousIndex(selectedCameraIndex));
	}

	/**
	 * Get the previous valid index in the camera list. If the current index is
	 * the first one, returns the last.
	 * 
	 * @param currentIndex
	 */
	private int previousIndex(int currentIndex) {
		if (currentIndex <= 0) {
			if (indexesBlackList.contains(systemCams.length - 1))
				return previousIndex(systemCams.length - 1);
			else
				return systemCams.length - 1;
		}
		if (indexesBlackList.contains(currentIndex - 1))
			return previousIndex(--currentIndex);
		else
			return --currentIndex;
	}

	/**
	 * Selects a camera with a resolution that is higher then the current
	 * resolution. If the current camera is the best, jumps to the worst.
	 * 
	 * @param processing
	 *            - PApplet instance
	 */
	public void previousCameraResolution() {
		up = false;

		if (this.selectedResolution + 1 >= 0) {
			this.selectedResolution = VideoResolutions.getHighestResolutionIndex();
			if (indexesBlackList.contains(this.selectedResolution)){
				previousCameraResolution();
			} else {				
				setCamera(this.selectedResolution);
			}			
		} else {
			++this.selectedResolution;
			if (this.indexesBlackList.contains(this.selectedResolution)){
				previousCameraResolution();
			} else {
				setCamera(this.selectedResolution);
			}
		}
	}

	/**
	 * Selects a camera with a resolution that is lower then the current
	 * resolution. If the current camera is the worst, jumps to the best.
	 * 
	 * @param processing
	 *            - PApplet instance
	 */
	public void nextCameraResolution() {
		up = true;

		if ((this.selectedResolution - 1) < VideoResolutions.getHighestResolutionIndex()) {
			this.selectedResolution = VideoResolutions.getLowestResolutionIndex();
			if (indexesBlackList.contains(this.selectedResolution))
				nextCameraResolution();
			else
				setCamera(this.selectedResolution);
		} else {
			--this.selectedResolution;
			if (indexesBlackList.contains(this.selectedResolution))
				nextCameraResolution();
			else
				setCamera(this.selectedResolution);
		}
	}

	/**
	 * Retry to load a resolution in case of failure. If I'm not blocking 
	 * failing cameras, this will not help. In this cases, log the error and
	 * load a random camera within the available cameras.  
	 */
	public void retryResolution() {
		
		if (blacklistSettings == BLACKLIST_BLOCK_UPDATES ||
				blacklistSettings == BLACKLIST_IGNORE)
		{
			// Yes, random!
			Random generator = new Random(); 
			setCamera(generator.nextInt(systemCams.length));
		} else {
			setCamera(this.selectedResolution);
		}
	}

	/**
	 * 
	 * @param screenSize
	 *            Screen size as 'width'x'height' with no white spaces
	 * @param cameras
	 *            List of all cameras of the system
	 * @return the index of the "best match", the screen size of passed with the
	 *         highest frame rate. Returns -1 if the screen size could not find
	 *         a single match.
	 */
	private int matchScreenSize(String[] cameras, String screenSize) {
		int selected = -1;
		int bestFrameRate = 0;
		// selected = Integer.parseInt("1");

		for (int i = 0; i < cameras.length; i++) {
			if (cameras[i].contains(screenSize)) {
				// The characters after the '=' correspond to the frame rate.
				int firstIndexOfFPS = cameras[i].lastIndexOf('=') + 1;
				int frameRate = Integer.parseInt(cameras[i].substring(firstIndexOfFPS));
				if (frameRate > bestFrameRate && !indexesBlackList.contains(i)) {
					selected = i;
					bestFrameRate = frameRate;
				}
			}
		}
		
		System.out.println("Devices' bestFrameRate = " + bestFrameRate);
		
		return selected;
	}

	/**
	 * Get the selected camera's height and the width in pixels and returns it
	 * in an array.
	 */
	public Point<Integer> getScreenSize() {
		String camera = systemCams[this.selectedCameraIndex];

		int[] result = new int[2];
		int indexOfFirst_eqSign_ = camera.indexOf('=');
		int indexOfSecond_eqSign_ = camera.indexOf('=', indexOfFirst_eqSign_ + 1);
		int indexOf_x_ = camera.indexOf('x');
		int indexOfLast_comma_ = camera.lastIndexOf(',');

		result[0] = Integer.parseInt(camera.substring(indexOfSecond_eqSign_ + 1, indexOf_x_));
		result[1] = Integer.parseInt(camera.substring(indexOf_x_ + 1, indexOfLast_comma_));

		// System.out.println("The size of each frame is " + result[0] + " x " +
		// result[1] + " px");

		return new Point<Integer>(result[0], result[1]);
	}

	/**
	 * Receive information from client that the current camera is not working
	 * and put its index on the blacklist.
	 */
	public void informFailure() {
		
		
		if (blacklistSettings != BLACKLIST_IGNORE && 
				blacklistSettings != BLACKLIST_BLOCK_UPDATES)
		{
			addIndexToBlacklist(this.selectedCameraIndex);
		}
			
		
		// nextCamera(this.processing);
		System.out.println("blacklist: " + getStringFromListOfIntegers());
	}

	private String getStringFromListOfIntegers() {
		return listOfIntegersToString(indexesBlackList);
	}
	
	private String listOfIntegersToString(List<Integer> _list){
		String result = "{  ";
		for (Integer i : _list) {
			result += i + "  ";
		}
		return result + "}";
	}
	
	private static void logAllCameras(String[] cameras, List<Integer> indexesBlackList, int selectedCameraIndex) {
		
		System.out.println("Available cameras:");
		for (int i = 0; i < cameras.length; i++) {
			String selected = " ";
			if (i == selectedCameraIndex) {
				selected = "*";
			} else if (indexesBlackList.contains(i)) {
				selected = "-";
			}
			System.out.println("[" + selected + "] " + i + ". " + cameras[i]);		
		}
	}
	
	public static void logAllCameras(String[] cameras) {
		logAllCameras(cameras, new ArrayList<Integer>(), -1);
	}
	
	private void loadBlacklist(){
		
		// Cleans up the file if it exists. Uses a clean sheet
		if (this.blacklistSettings == BLACKLIST_RESET){
			deleteFile(CACHE_DIR, BLACKLIST_FILENAME);
		}
		
		// Loads the existing blacklist in "/bin/CACHE_DIR"
		if (this.blacklistSettings == BLACKLIST_ALLOW_UPDATES
				|| blacklistSettings == BLACKLIST_BLOCK_UPDATES 
				&& isCameraDataValid()){
			this.indexesBlackList = getListOfIntFromFile(CACHE_DIR, BLACKLIST_FILENAME);
			System.out.println("Loaded blacklist: " + this.indexesBlackList);
		}
		
	}
	
	@SuppressWarnings("resource")
	private List<String> getListOfStringsFromFile(String path, String filename){
		List<String> _list = new ArrayList<>(); 
		FileReader in = null;

		try {

			in = new FileReader(path+File.separator+filename);
			
			if (path.equals("")){
				in = new FileReader(filename);
			} else {
				in= new FileReader(path+File.separator+filename);
			}
			
			Scanner input = new Scanner(in);
			
			while (input.hasNext()) {
				String s = input.nextLine();
				if (!s.equals(""))
					_list.add(s);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return _list;
	}
	
	@SuppressWarnings("resource")
	private List<Integer> getListOfIntFromFile(String path, String filename){
		List<Integer> _list = new ArrayList<>();
		FileReader in = null;

		try {

			in = new FileReader(path+File.separator+filename);
			
			if (path.equals("")){
				in = new FileReader(filename);
			} else {
				in= new FileReader(path+File.separator+filename);
			}
			
			Scanner input = new Scanner(in);
			
			while (input.hasNext()) {
				_list.add(input.nextInt());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return _list;
	}
	
	private void addAnIntToAFile(String path, String filename, int intToAdd){
		
		BufferedWriter out = null;

		try {
			
			File _filename;
			
			if (path.equals("")){
				_filename = new File(filename);
			} else {
				_filename = new File(path+File.separator+filename);
			}
			
			if (_filename.exists()){
				out = new BufferedWriter(new FileWriter(_filename, true));
				out.newLine();
				out.append(Integer.toString(intToAdd));
			} else {
				out = new BufferedWriter(new FileWriter(_filename));
				out.write(Integer.toString(intToAdd));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void storeAnArrayOfStringsToAFile(String path, String filename, String[] strings){
		
		BufferedWriter out = null;

		try {
			
			File _filename;
			
			if (path.equals("")){
				_filename = new File(filename);
			} else {
				_filename = new File(path+File.separator+filename);
			}
			
			
			out = new BufferedWriter(new FileWriter(_filename));
			
			for (String s : strings){
				out.write(s);
				out.newLine();
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void addIndexToBlacklist(int newIndexToBlock){
		
		this.indexesBlackList.add(newIndexToBlock);
		
		// Check if 
		if (blacklistSettings == BLACKLIST_RESET 
				|| blacklistSettings == BLACKLIST_ALLOW_UPDATES){
			
			addAnIntToAFile(CACHE_DIR, BLACKLIST_FILENAME, newIndexToBlock);
			
		} else {
			// This should not be called
			System.out.println("Inclusion not allowed.");
		}
	}
	
	private void deleteFile(String path, String filename){
		try{
			File _filename;
			
			if (path.equals("")){
				_filename = new File(filename);
			} else {
				_filename = new File(path+File.separator+filename);
			}
			_filename.delete();
		} catch (Exception e){
			e.printStackTrace();
		}
	}
}
