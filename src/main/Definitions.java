package main;
public final class Definitions {
	
	private Definitions () {}
	
	public static final float sectors_minPortion_createBody    = 0.0015f;//0.0028f; // a body is created if we change more than minPortion_createBody*numberOfSectors sectors 
	public static final float sectors_minPortion_force1Cluster = 0.3f; // we force to a unique body if we change more than minPortion_force1Cluster*numberOfSectors sectors
	public static final float residualLevel                    = 10.f * 0.005f;		
	public static final int   body_age_to_last                 = 3;
}
