package main;

import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Field;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import osc.OscCom;
import osc.PureData;
import processing.core.PApplet;
import sectors.MatrixSize;
import sectors.Point;
import sectors.SectorBuilder;

/**
 * This class provides a simple way to abstract the SoMoConfig creation and hide its complexities.
 * @author marcos
 *
 */
public class SoMoConfigBuilder implements ISoMoConfigBuilder {
	
	private SoMoConfig config;

	public SoMoConfigBuilder() {
		this.config = new SoMoConfig();
	}
	
	/*@Override
	public SoMoConfigBuilder setCameraByVideoResolution(int resolution) throws Exception {
		
		if (resolution >= 0){
			throw new Exception("Not a valid video resolution. Use VideoResolution constants."
					+ " Are your trying to use setCameraByIndex?");
		}
		
		config.videoResolution = resolution;
		return this;
	}
	
	@Override
	public SoMoConfigBuilder setCameraByIndex(int cameraIndex) throws Exception {
		
		if (cameraIndex < 0){
			throw new Exception("Not a valid camera index, see the index list in console."
					+ "Are your trying to use setCameraByVideoResolution?");
		}
		
		config.videoResolution = cameraIndex;
		return this;
	}*/
	
	@Override
	public SoMoConfigBuilder setSectorsMatrixSize(int rows, int columns){
		config.sectorsMatrixSize = new MatrixSize(rows, columns);
		config.sectorsCount = rows * columns;
		return this;
	}
	
	@Override
	public SoMoConfigBuilder setSkippingPixels(int horizontalPixels, int verticalPixels) {
		config.skippingPixels = new Point<>(horizontalPixels, verticalPixels);
		return this;
	}
	
	@Override
	public SoMoConfigBuilder setDisplayBodyPolygons(int displayBodyPolygons) {
		config.displayBodyPolygons = displayBodyPolygons;
		return this;
	}
	
	@Override
	public SoMoConfigBuilder setMaxSectorAccumulation (byte maxSectorAccumulation) {
		config.maxSectorAccumulation = maxSectorAccumulation;
		return this;
	}
	
	@Override
	public SoMoConfigBuilder setSectorAccumulationFraction (int sectorAccumulationFraction) {
		config.sectorAccumulationFraction = sectorAccumulationFraction;
		return this;
	}
	
	@Override
	public SoMoConfigBuilder setLocalCommunication(String messageName, String messageName_manual, String ipAddress, int port) {
		config.oscMessageName = messageName;
		config.oscMessageName_manual = messageName_manual;
		config.oscLocalhostIpAddress = ipAddress;
		config.oscLocalhostPort = port;
		return this;
	}
	
	@Override
	public ISoMoConfigBuilder setPixelChangeThreshold(int threshold) {
		config.pixelChangeThreshold = threshold;
		return this;
	}
	
	public SoMoConfigBuilder readConfigurationFile(String resourceFilename) throws Exception {
		ConfigData configData = null;

		try(java.io.BufferedReader reader = new java.io.BufferedReader(
				new InputStreamReader( SoMoConfigBuilder.class.getResourceAsStream(resourceFilename)/*, "UTF-8"*/ )
					))
		//try(Reader reader = new InputStreamReader(SoMoConfigBuilder.class.getResourceAsStream(filename), "UTF-8")){ // this was for files in file system
		{
			Gson gson = new GsonBuilder().create();
			configData = gson.fromJson(reader, ConfigData.class);
			reader.close();
		}
		catch (Exception ex) {
        	ex.printStackTrace();
        }
		
		/*	old code: videoResolution and videoResolutionIndex as separate variables
		int videoResolutionIndex = -1;
		if (configData.videoResolution != null){
			videoResolutionIndex = getIndexByResolution(configData.videoResolution);
			setCameraByVideoResolution(videoResolutionIndex);
		} else {
			setCameraByIndex(configData.videoIndex);
		}*/
		System.out.println("configData.videoResolution_or_videoDeviceIndex = " + configData.videoResolution_or_videoDeviceIndex);
		if (configData.videoResolution_or_videoDeviceIndex != null){
			if(configData.videoResolution_or_videoDeviceIndex.charAt(0) > 47 && configData.videoResolution_or_videoDeviceIndex.charAt(0) < 58 )
			{
				System.out.println("Integer.parseInt(configData.videoResolution_or_videoDeviceIndex) = " + Integer.parseInt(configData.videoResolution_or_videoDeviceIndex));
				config.videoResolution_or_videoDeviceIndex = Integer.parseInt(configData.videoResolution_or_videoDeviceIndex);
			}
			else {
				System.out.println("getIndexByResolution(configData.videoResolution_or_videoDeviceIndex) = " + getIndexByResolution(configData.videoResolution_or_videoDeviceIndex));
				int videoResolutionIndex = getIndexByResolution(configData.videoResolution_or_videoDeviceIndex);
				config.videoResolution_or_videoDeviceIndex = videoResolutionIndex;
			}
		} else {
			config.videoResolution_or_videoDeviceIndex = 0;
		}
		
				
		setSectorsMatrixSize(configData.sectorsMatrixSize.rows, configData.sectorsMatrixSize.columns);
		setPixelChangeThreshold(configData.pixelChangeThreshold);
		setSkippingPixels(configData.skippingPixels.x, configData.skippingPixels.y);
		setDisplayBodyPolygons(configData.displayBodyPolygons);
		setMaxSectorAccumulation(configData.maxSectorAccumulation);
		setSectorAccumulationFraction(configData.sectorAccumulationFraction);
		
		System.out.println("configData.pixelChangeThreshold = " + configData.pixelChangeThreshold);
		System.out.println("configData.displayBodyPolygons = " + configData.displayBodyPolygons);
		System.out.println("configData.sectorAccumulationFraction = " + configData.sectorAccumulationFraction);
		
		System.out.println("configData.oscMessageName = " + configData.oscMessageName);
		System.out.println("configData.oscMessageName_manual = " + configData.oscMessageName_manual);
		System.out.println("configData.oscIpAddress = " + configData.oscLocalhostIpAddress);
		System.out.println("configData.oscPort = " + configData.oscLocalhostPort);
		
		setLocalCommunication(configData.oscMessageName, configData.oscMessageName_manual, configData.oscLocalhostIpAddress, configData.oscLocalhostPort);
		
		config.oscWatcherIpAddress = configData.oscWatcherIpAddress;
		config.oscWatcherPort = configData.oscWatcherPort;
		config.oscWatcherMsgName_100_over_FPS = configData.oscWatcherMsgName_100_over_FPS;
		config.oscWatcherMsgName_AccFraction_over_30 = configData.oscWatcherMsgName_AccFraction_over_30;
		config.oscWatcherMsgName_PixChangeThreshold_over_100 = configData.oscWatcherMsgName_PixChangeThreshold_over_100;
		config.oscWatcherMsgName_Distorter = configData.oscWatcherMsgName_Distorter;
		config.oscWatcherMsgName_Uivo = configData.oscWatcherMsgName_Uivo;
		config.oscWatcherMsgName_LevelMultiplier = configData.oscWatcherMsgName_LevelMultiplier;
		
		System.out.println("configData.oscWatcherIpAddress = " + configData.oscWatcherIpAddress);
		System.out.println("configData.oscWatcherPort = " + configData.oscWatcherPort);
		
		System.out.println("configData.oscWatcherMsgName_100_over_FPS = " + configData.oscWatcherMsgName_100_over_FPS);
		System.out.println("configData.oscWatcherMsgName_AccFraction_over_30 = " + configData.oscWatcherMsgName_AccFraction_over_30);
		System.out.println("configData.oscWatcherMsgName_PixChangeThreshold_over_100 = " + configData.oscWatcherMsgName_PixChangeThreshold_over_100);
		System.out.println("configData.oscWatcherMsgName_Distorter = " + configData.oscWatcherMsgName_Distorter);
		System.out.println("configData.oscWatcherMsgName_Uivo = " + configData.oscWatcherMsgName_Uivo);
		System.out.println("configData.oscWatcherMsgName_LevelMultiplier = " + configData.oscWatcherMsgName_LevelMultiplier);
		
		config.oscSectorsPolysReceiver_IpAddress = configData.oscSectorsPolysReceiver_IpAddress;
		config.oscSectorsPolysReceiver_Port = configData.oscSectorsPolysReceiver_Port;
		config.oscSectorsPolysReceiver_MsgName_ONOFF = configData.oscSectorsPolysReceiver_MsgName_ONOFF;
		config.oscSectorsPolysReceiver_MsgName_Accum = configData.oscSectorsPolysReceiver_MsgName_Accum;
		
		System.out.println("configData.oscSectorsPolysReceiver_IpAddress = " + configData.oscSectorsPolysReceiver_IpAddress);
		System.out.println("configData.oscSectorsPolysReceiver_Port = " + configData.oscSectorsPolysReceiver_Port);
		System.out.println("configData.oscSectorsPolysReceiver_MsgName_ONOFF = " + configData.oscSectorsPolysReceiver_MsgName_ONOFF);
		System.out.println("configData.oscSectorsPolysReceiver_MsgName_Accum = " + configData.oscSectorsPolysReceiver_MsgName_Accum);
		
		return this;
	}
	
	@Override
	public SoMoConfig build(PApplet processing) {
		config.processing = processing;
		System.out.println("camera blacklist settings hardcoded to BLACKLIST_IGNORE(" + CameraCapture.BLACKLIST_IGNORE + ")");
		config.camera = new CameraCapture(processing, config.videoResolution_or_videoDeviceIndex, CameraCapture.BLACKLIST_IGNORE);
		config.oscLocalhost = new OscCom(processing, config.oscLocalhostIpAddress, config.oscLocalhostPort);		
		config.pd = new PureData(config.oscLocalhost, config.oscMessageName, config.oscMessageName_manual);
		config.watcherOSC = new OscCom(processing, config.oscWatcherIpAddress, config.oscWatcherPort);
		config.updateScreenSize();
		SectorBuilder.createSectors(config);
		return config;
	}
	
	///	NOTE - I think it may be too much confusing to keep both a videoResolution and a videoIndex
	///variables. If both are present in the json, videoIndex gets ignored, and that's not intuitive.
	///	And, CameraCapture uses only one variable... [Brizo] 5/5/2017
	public class ConfigData {
		public MatrixSize sectorsMatrixSize;
		public Point<Integer> skippingPixels;
		public String videoResolution_or_videoDeviceIndex;
		//public int videoIndex;
		public int displayBodyPolygons; 
		public byte maxSectorAccumulation;
		public int sectorAccumulationFraction;
		public int pixelChangeThreshold;
		//	These are for OSC sent via localhost to the synth
		public String oscMessageName;
		public String oscMessageName_manual;
		public String oscLocalhostIpAddress;		
		public int oscLocalhostPort;
		//	To send the sectors via OSC as an image:
		public String oscSectorsPolysReceiver_IpAddress;
		public int oscSectorsPolysReceiver_Port;
		public String oscSectorsPolysReceiver_MsgName_ONOFF;
		public String oscSectorsPolysReceiver_MsgName_Accum;
		//	These are for OSC sent to an external watcher (Android's app Control, for example)
		public String oscWatcherIpAddress;		
		public int oscWatcherPort;
		public String oscWatcherMsgName_100_over_FPS;
		public String oscWatcherMsgName_AccFraction_over_30;
		public String oscWatcherMsgName_PixChangeThreshold_over_100;
		public String oscWatcherMsgName_Distorter;
		public String oscWatcherMsgName_Uivo;
		public String oscWatcherMsgName_LevelMultiplier;
	}
	
	private int getIndexByResolution(String fieldName) throws NoSuchFieldException, 
	SecurityException, IllegalArgumentException, IllegalAccessException {
		VideoResolutions resolutions = new VideoResolutions();
		Field field = resolutions.getClass().getDeclaredField(fieldName);
	    return field.getInt(resolutions);
	}
}