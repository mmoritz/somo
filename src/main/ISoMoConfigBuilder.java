package main;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import processing.core.PApplet;

public interface ISoMoConfigBuilder {
	/**
	 * The matrix of big pixels the screen will be divided into 
	 * @param rows 
	 * @param columns
	 * @return
	 */
	ISoMoConfigBuilder setSectorsMatrixSize(int rows, int columns);
	
	/**
	 * Tries to match automatically the resolution passed with the available cameras in your hardware.
	 * Always try the highest frame rate first.
	 * @param resolution provided by the constants in VideoResolutions class
	 * @return
	 * @throws Exception if a invalid number is passed in
	 */
	//ISoMoConfigBuilder setCameraByVideoResolution(int resolution) throws Exception;
	
	/**
	 * Tries to use the camera index passed as argument. The list of available cameras is provided in console.
	 * @param cameraIndex
	 * @return
	 * @throws Exception
	 */
	//ISoMoConfigBuilder setCameraByIndex(int cameraIndex) throws Exception;
	
	
	ISoMoConfigBuilder setSkippingPixels(int horizontalPixels, int verticalPixels);
	
	ISoMoConfigBuilder setDisplayBodyPolygons(int displayBodyPolygons);
	
	/**
	 * Configures communication via OSC protocol
	 * @param messageName
	 * @param ipAddress
	 * @param port
	 * @return
	 */
	ISoMoConfigBuilder setLocalCommunication(String messageName, String oscMessageName_manual, String ipAddress, int port);
	
	/**
	 * ...
	 * @param maxSectorAccumulation
	 * @return
	 */
	ISoMoConfigBuilder setMaxSectorAccumulation (byte maxSectorAccumulation);
	
	
	ISoMoConfigBuilder setSectorAccumulationFraction (int sectorAccumulationFraction);
	
	/**
	 * Use JSON configuration file for application setup
	 * @param filename
	 * @return
	 * @throws Exception 
	 */
	ISoMoConfigBuilder readConfigurationFile (String filename) throws Exception;
		
	/**
	 * Use JSON configuration file for application setup
	 * @param threshold
	 * @return
	 * @throws Exception 
	 */
	ISoMoConfigBuilder setPixelChangeThreshold (int threshold);
	
	/**
	 * Generates the configuration holder object.
	 * @param processing
	 * @return
	 * @throws IOException 
	 * @throws UnsupportedEncodingException 
	 * @throws Exception 
	 */
	SoMoConfig build(PApplet processing);	
}
