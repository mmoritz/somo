package main;

import osc.OscCom;
import osc.PureData;
import processing.core.PApplet;
import sectors.AnalysisSector;
import sectors.MatrixSize;
import sectors.Point;
import processing.core.PConstants;	//	RGB, HSB...

public class SoMoConfig {
	
	//public int videoResolution;
	public int videoResolution_or_videoDeviceIndex;
	public MatrixSize sectorsMatrixSize;
	public Point<Integer> skippingPixels;
	public int displayBodyPolygons;
	public byte maxSectorAccumulation = AnalysisSector.DEFAULT_MAX_ACCUMULATION;
	public int sectorAccumulationFraction = AnalysisSector.DEFAULT_ACCUMULATION_FRACTION;
	public CameraCapture camera;
	//	These are for local communication with the synth
	public OscCom oscLocalhost;
	public PureData pd;
	public String oscMessageName = "/somo";
	public String oscMessageName_manual = "/somo_manual";
	public String oscLocalhostIpAddress = "localhost";
	public int oscLocalhostPort = 56789;
	//	To send the sectors via OSC as an image:
	public String oscSectorsPolysReceiver_IpAddress = "localhost";
	public int oscSectorsPolysReceiver_Port = 8081;
	public String oscSectorsPolysReceiver_MsgName_ONOFF = "/sectorsON_OFF";
	public String oscSectorsPolysReceiver_MsgName_Accum = "/sectorsAccum";
	//	And these are for a watcher 
	public String oscWatcherIpAddress = "localhost";
	public int oscWatcherPort = 8080;
	public OscCom watcherOSC;
	public String oscWatcherMsgName_100_over_FPS = "/vol4";
	public String oscWatcherMsgName_AccFraction_over_30 = "/vol1";
	public String oscWatcherMsgName_PixChangeThreshold_over_100 = "/vol2";
	public String oscWatcherMsgName_Distorter = "/solo6";
	public String oscWatcherMsgName_Uivo = "/mute6";
	public String oscWatcherMsgName_LevelMultiplier = "/vol6";
	
	public int pixelChangeThreshold;
	
	public int sectorsCount;
	public Point<Integer> screenSize;
	public PApplet processing;
	
	public int colorMode = PConstants.HSB;
	
	
	//SoMoConfig() { }
	
	public void updateScreenSize() {
		screenSize = camera.getScreenSize();
		processing.size(screenSize.x, screenSize.y);
	}
}