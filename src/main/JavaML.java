package main;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

//import processing.core.PApplet;
import net.sf.javaml.clustering.Clusterer;
import net.sf.javaml.clustering.KMeans;
import net.sf.javaml.clustering.evaluation.ClusterEvaluation;
import net.sf.javaml.clustering.evaluation.SumOfSquaredErrors;
import net.sf.javaml.core.Dataset;
import net.sf.javaml.core.DefaultDataset;
import net.sf.javaml.core.Instance;
import net.sf.javaml.core.SparseInstance;
//import net.sf.javaml.tools.InstanceTools;
import net.sf.javaml.tools.data.FileHandler;


// Machine Learning Library - Thomas Abeel
// http://java-ml.sourceforge.net/content/getting-started

public class JavaML {
	
	public static ArrayList<float[][]> runKMeans(float[][] rawData, int maxK){
		
		if (rawData.length > 0){
			
//			System.out.println("dataset.length: " + rawData.length);
			
			Dataset data = arrayListOf2DArrayOfFloatsToDataset(rawData);
			
//			printDatasetToFile(data);
			
			return arrayOfDatasetsToArrayListOf2DArrayOfFloats(runKMeans(data, maxK));
		}
		
		return null;
	}
	
	static Dataset arrayListOf2DArrayOfFloatsToDataset(float[][] rawData){
		
		int m = rawData.length;
		int n = rawData[0].length;
		
		Dataset data = new DefaultDataset();
		
		for (int i = 0; i<m; i++){
			
			Instance instance = new SparseInstance(n);
			
			for (int j=0;j<n;j++){
				instance.put(j, (double) rawData[i][j]);
			}
			
			data.add(instance);
		}
		 		
		return data;
	}
	
	@SuppressWarnings("deprecation")
	public static ArrayList<float[][]> arrayOfDatasetsToArrayListOf2DArrayOfFloats
	(Dataset[] clusters) {
		ArrayList<float[][]> convertedData = new ArrayList<float[][]>();
		
		// Iterate through Datasets
		for (int i=0; i<clusters.length;i++){
			
			if (clusters[i].size() > 0 && clusters[i].get(0).size() > 0){
				float[][] tmpSet = new float[clusters[i].size()][clusters[i].get(0).size()];
//				System.out.println("float[" + tmpSet.length + "]["+ clusters[i].get(0).size() +"]");
				
				// Iterate through Instances
				for (int j=0; j<clusters[i].size(); j++){

					// Iterate through key-value pairs
					for (int k=0; k<clusters[i].get(0).size();k++){
						tmpSet[j][k] = (float) (double) clusters[i].get(j).get(k);
					}
				}
				
				convertedData.add(tmpSet);
			}
			
		}
		
		
		// Debug
//		if (convertedData.size() > 0){
//			// Debug
//			for (int i=0;i<convertedData.size();i++){
//				for (int j=0;j<convertedData.get(i).length;j++){
//					for (int k=0;k<convertedData.get(i)[j].length;k++){
//						System.out.println("convertedData["+i + ","+ j+","+k+"]: " 
//								+ convertedData.get(i)[j][k]);
//					}
//				}
//			}		
//		}
		
	
		return convertedData;
	}
	
	public static Dataset[] runKMeans(Dataset data, int maxK){

		double sumOfSquaredErrors[] = new double [maxK];
		
		for (int k=1; k<maxK+1; k++){

			// Debug and get times
//			long startTime = System.currentTimeMillis();
			Clusterer km = new KMeans(k);
			/*
			 * Cluster the data, it will be returned as an array of data sets, with
			 * each dataset representing a cluster
			 */
			Dataset[] clusters = km.cluster(data);
//			long stopTime = System.currentTimeMillis();
//			long elapsedTime = stopTime - startTime;
//			System.out.print("K = " + clusters.length + ", time: " + elapsedTime + "ms");
			
//			startTime = System.currentTimeMillis();
			sumOfSquaredErrors[k-1] = getSumOfSquaredErrors2(clusters);
//			stopTime = System.currentTimeMillis();
//			elapsedTime = stopTime - startTime;
//			
//			System.out.println(", sse2: " + sumOfSquaredErrors[k-1] + ", time: " + elapsedTime);

			// to open a data file saved with FileHandler.exportDataset(data, file):
			// Dataset data = FileHandler.loadDataset(new File("output.txt"), 0,"\t");

		}
		
		// -------------Pick the best K-------------
		// I'm using the following reasoning:
		// - Situation 1: sparse points evenly distributed
		// - If I have 2 clusters in a 1d space the sse 
		// 		should be 0.25 of sse with 1 cluster
		// - If I have 3 clusters in a 1d space the sse 
		// 		should be 0.44 of sse with 3 clusters
		// ... the array that discribes these numbers are:
//		double[] minToWorkKIncrement = { 1, 0.25, 0.44, 0.56, 0.64, 0.69, 0.73, 0,77 }; // related to last
//		double[] minToWorkKIncrement = { 1, 0.25, 0.11, 0.063, 0.04, 0.028, 0.02, 0.016 }; // related to K = 1
		double[] minToWorkKIncrement = { 1, 4, 9, 16, 25, 36, 49, 64 }; // related to K = 1
		
		double[] relativeSse = new double[sumOfSquaredErrors.length];
		
		
		int bestK = 1;
		for (int k=1; k<maxK+1; k++){
			
			relativeSse[k-1] = sumOfSquaredErrors[k-1] * minToWorkKIncrement[k-1];
//			System.out.println("relativeSSE["+ k +": " + relativeSse[k-1]);
			
			if (k==1 || relativeSse[k-1] < relativeSse[k-2]){
				bestK = k;
			}
		}
		
//		System.out.println("The selected k was: "+ bestK);
		
		
		Clusterer km = new KMeans(bestK);
		return km.cluster(data);
	}
	
	
	public static void printClustersToFile(Dataset[] clusters, String prefixName){
		
		int numberOfClusters = clusters.length;  
		
		// Not saving in a Java Applet, it works with java applications...
		for (int i=0; i<numberOfClusters; i++){
			printDatasetToFile(clusters[i], prefixName + numberOfClusters 
					+ "-" + i + ".txt");
		}
	}
	
	public static void printClustersToFile(Dataset[] clusters){

		int numberOfClusters = clusters.length;  

		// Not saving in a Java Applet, it works with java applications...
		for (int i=0; i<numberOfClusters; i++){
			printDatasetToFile(clusters[i], "output" + numberOfClusters 
					+ "-" + i + ".txt");
		}
	}
	
	
	public static void printDatasetToFile(Dataset data, String filename){
		try {
			FileHandler.exportDataset(data,new File(filename));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void printDatasetToFile(Dataset data){
		printDatasetToFile(data, "output.txt");
	}
	
	public static void testKMeans(Dataset data, int maxK){

		for (int k=1; k<maxK+1; k++){

			// Debug and get times
			long startTime = System.currentTimeMillis();
			Clusterer km = new KMeans(k);
			/*
			 * Cluster the data, it will be returned as an array of data sets, with
			 * each dataset representing a cluster
			 */
			Dataset[] clusters = km.cluster(data);
			long stopTime = System.currentTimeMillis();
			long elapsedTime = stopTime - startTime;
			System.out.print("K = " + clusters.length + ", time: " + elapsedTime + "ms");
			
			
			startTime = System.currentTimeMillis();
			double sse = getSumOfSquaredErrors(clusters);
			stopTime = System.currentTimeMillis();
			elapsedTime = stopTime - startTime;
			
			System.out.print(", sse: " + sse + ", time: " + elapsedTime);
			
			startTime = System.currentTimeMillis();
			double sse2 = getSumOfSquaredErrors2(clusters);
			stopTime = System.currentTimeMillis();
			elapsedTime = stopTime - startTime;
			
			System.out.println(", sse2: " + sse2 + ", time: " + elapsedTime);
			
//			System.out.println("clusters[0].get(0): " + clusters[0].get(0)); 

			
			// Not saving in a Java Applet, it works with java applications...
			printClustersToFile(clusters);

			// to open a data file saved with FileHandler.exportDataset(data, file):
			// Dataset data = FileHandler.loadDataset(new File("output.txt"), 0,"\t");

		}
	}
	
	public static double getSumOfSquaredErrors (Dataset[] clusters){
		ClusterEvaluation sse= new SumOfSquaredErrors();
		/* Measure the quality of the clustering */
		return sse.score(clusters);
	}
	
	public static double getSumOfSquaredErrors2(Dataset[] clusters){
		
		int k = clusters.length;
		
//------------------------------------------
		// Calculate centroids
		Point[] centroid = new Point[k];
		
		for (int i=0; i<k; i++){
			
			// creates a point with x=y=0
			centroid[i] = new Point();
			
			for (int j=0; j<clusters[i].size();j++){
				centroid[i].x += clusters[i].get(j).get(0);
				centroid[i].y += clusters[i].get(j).get(1);
			}
			
			centroid[i].x /= clusters[i].size();
			centroid[i].y /= clusters[i].size();
			
//			System.out.println(centroid[i].toString());
		}
		
//-------------Get the errors------------------
		
		double sse = 0.;
		
		for (int i=0; i<k; i++){
			for (int j=0; j<clusters[i].size();j++){
				sse += Math.pow(centroid[i].x - clusters[i].get(j).get(0), 2);
				sse += Math.pow(centroid[i].y - clusters[i].get(j).get(1), 2);
			}
		}
		
		
		return 2*sse;
	}
}
