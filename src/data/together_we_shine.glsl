#ifdef GL_ES
precision mediump float;
#endif

#define PROCESSING_COLOR_SHADER

uniform vec2 u_resolution;
uniform vec3 u_mouse;
uniform float u_time;

void main() {
    vec2 st = gl_FragCoord.st/u_resolution;
    gl_FragColor = vec4(st.x,st.y,0.0,1.0);
}


#ifdef seiouthmnoeriuthncoseruitnoer 

//https://www.shadertoy.com/view/4d3XDS (Together we shine)

#ifdef GL_ES
precision highp float;
#endif

// Type of shader expected by Processing
#define PROCESSING_COLOR_SHADER

// Processing specific input
//uniform float time;
uniform vec2 resolution;
//00uniform vec2 mouse;


/*
uniform vec3      iResolution;           // viewport resolution (in pixels)
uniform float     iGlobalTime;           // shader playback time (in seconds)
uniform float     iTimeDelta;            // render time (in seconds)
uniform int       iFrame;                // shader playback frame
uniform float     iChannelTime[4];       // channel playback time (in seconds)
uniform vec3      iChannelResolution[4]; // channel resolution (in pixels)
uniform vec4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click
uniform samplerXX iChannel0..3;          // input channel. XX = 2D/Cube
uniform vec4      iDate;                 // (year, month, day, time in seconds)
uniform float     iSampleRate;           // sound sample rate (i.e., 44100)
*/

// Layer between Processing and Shadertoy uniforms
vec3 iResolution = vec3(resolution,0.0);
//float iGlobalTime = time;
//vec4 iMouse = vec4(mouse,0.0,0.0); // zw would normally be the click status

float circle(vec2 i) {
    return length(i);
}

float square(vec2 i) {
    i = abs(i);
    return max(i.x,i.y);
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	vec2 uv1 = (fragCoord.xy-iResolution.xy*.5) / iResolution.y;
    vec2 uv = uv1*5.0;
    
    float c = 1.0;
    //if (iMouse.w>0.5)
    //    c = texture(iChannel1,uv-iMouse.xy*5.0/iResolution.xy).r*1.2-.1;
    float gt = iGlobalTime*.5;
    for (float i = 0.0; i< 20.0; i++) {
      gt += i*1.72;
      c *= clamp(square(1.6*(uv+
                            vec2(/*sin(gt*0.9)*2.+cos(gt*(i/37.))*/i/2.5 - 4.
                                ,1./*sin(gt*0.7)+cos(gt*(i/23.))*/)))-.18,0.0,1.7);
    }
    
	fragColor = vec4(clamp(
                       mix( vec3(1.7),
                            vec3(.7,.7,1.)
                           /**(0.5+texture(iChannel2,vec2(uv1.y*4.,uv.x)+uv1*vec2(7.0,.13)).rgr)*/,
                            smoothstep(.0,10.,sqrt(c))
                       /**.15*max(-.2,6.-length(uv1))*/)
                       /**.18*(5.-length(uv))*/
                       ,0.0,1.0)
                     ,1.0);
}


#endif