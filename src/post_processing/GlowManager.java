package post_processing;

import processing.core.PGraphics;
import processing.core.PApplet;
import processing.opengl.PShader;

public class GlowManager {

  public PShader blur, glowShader;
  PGraphics pass1, pass2;
  PGraphics glowCanvas;

  float ratioGlow;

  public void initGlow(PApplet parent, int width, int height, float ratioGlow) {

    blur = parent.loadShader("blur.glsl");
    blur.set("blurSize", 19);
    blur.set("sigma", 7.0f);

    this.ratioGlow = ratioGlow;

    glowShader = parent.loadShader("glow.glsl");

    pass1 = parent.createGraphics((int) (width * ratioGlow),
        (int) (height * ratioGlow), PApplet.OPENGL);
    pass1.noSmooth();

    pass2 = parent.createGraphics((int) (width * ratioGlow),
        (int) (height * ratioGlow), PApplet.OPENGL);
    pass2.noSmooth();

    glowCanvas = parent.createGraphics(width, height,
        PApplet.OPENGL);
  }

  public PGraphics dowGlow(PGraphics canvas) {


    // Applying the blur shader along the vertical direction
    blur.set("horizontalPass", 0);
    pass1.beginDraw();
    pass1.image(canvas, 0, 0, pass1.width, pass1.height);
    pass1.filter(blur);
    pass1.endDraw();

    // Applying the blur shader along the horizontal direction
    blur.set("horizontalPass", 1);
    pass2.beginDraw();
    pass2.image(pass1, 0, 0);
    pass2.filter(blur);
    pass2.endDraw();

    // glowShader.set("Sample0", src);
    glowShader.set("Sample1", canvas);

    glowCanvas.beginDraw();
    glowCanvas.background(0);
    glowCanvas.image(pass2, 0, 0, glowCanvas.width, glowCanvas.height);
    glowCanvas.shader(glowShader);
    glowCanvas.endDraw();

    return glowCanvas;
  }
}
