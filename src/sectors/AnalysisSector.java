package sectors;

// Java's Color class. Used to convert between RGB and HSB colorspaces
import java.awt.Color;

import processing.core.PConstants;	//	for RGB

//import processing.core.PGraphics;

import processing.core.PApplet;
//import processing.core.PImage;

public class AnalysisSector
{
	Point<Integer> center;
	Point<Integer> size;
	Point<Integer> analysisSize;
	Point<Integer> skippingPixelsForAnalysedPixel;
	int centralIndex;
	boolean isON;
	int currentColorMode;

	public static final boolean DEBUG_SHOW_ANALYSIS_PIXELS = false;
	public static final boolean DEBUG_ANALYSIS_SIZE = false;
	
	public static final byte DEFAULT_MAX_ACCUMULATION = 60;
	public static final int DEFAULT_ACCUMULATION_FRACTION = 10;
	
	/**
	 * Number of rows and columns in which the image will be divided.
	 */
	public static int rows = -1;
	public static int columns = -1;
	public static byte[] accumulations;
	public static byte [] states; // byte just because OSCP5 cannot send booleans
	public static AnalysisSector[] sectors;
	public static float [][] lastFrameColors;
	public static float [][] currentFrameColors;
	public static boolean mirror = true;

	PApplet processing;

 	int aux_color;

	int numOfAnalysisPixelsX, numOfAnalysisPixelsY;

	int npixels;
	int [] indexesOnOriginalImage;

	int displayColor;
	
	private float [] rgb_or_hsb;

	AnalysisSector(
			Point<Integer> center,
			Point<Integer> size,
			Point<Integer> analysisSize,
			Point<Integer> jump,
			int currentColorMode,
			PApplet processing)
	{
		this.currentColorMode = currentColorMode;
		commonConstructorActions(center, size, analysisSize, jump, processing);; 
	}
	
	void commonConstructorActions(
			Point<Integer>center,
			Point<Integer>size,
			Point<Integer>analysisSize,
			Point<Integer>jump,			
			PApplet processing){
		
		this.center = center;
		this.size = size;
		this.analysisSize = analysisSize;		
		this.skippingPixelsForAnalysedPixel = jump;

		this.processing = processing;

		if(analysisSize.x < 1) {
			analysisSize.x = 1;
			if (DEBUG_ANALYSIS_SIZE) {System.out.println("[LOG] analysisSize[0] set to 1");}
		}

		if(analysisSize.y < 1) {
			if(DEBUG_ANALYSIS_SIZE){System.out.println("[LOG] analysisSize[1] set to 1");}
			analysisSize.y = 1;
		}

		centralIndex = center.y*processing.width + center.x;
		if (DEBUG_ANALYSIS_SIZE) {System.out.println("center = " + center.x + " " + center.y);}

		if(DEBUG_ANALYSIS_SIZE) {System.out.println("analysisSize[0] = " + analysisSize.x + " analysisSize[1] = " + analysisSize.y);}

		//  counting how many pixels we'll have
		npixels = 0;
		for(int i=0; i<analysisSize.y; i+=jump.y) { //  lines
			for(int j=0; j<analysisSize.x; j+=jump.x) {  //  columns
				npixels++;
			}
		}
		indexesOnOriginalImage = new int[npixels];
		int startIndex = 
				(center.y-analysisSize.y/2)*processing.width + center.x - analysisSize.x/2;
		if (DEBUG_ANALYSIS_SIZE) {System.out.println("startIndex = " + startIndex);}
		int count = 0;
		for(int i=0; i<analysisSize.y; i+=jump.y)  //  lines
		{
			for(int j=0; j<analysisSize.x; j+=jump.x)  //  columns
			{
				indexesOnOriginalImage[count] = startIndex + processing.width*i + j;
				count++;
			}
		}

		rgb_or_hsb = new float[3];

		isON = true;		
	}
	
	

	public Point<Integer> getSize(){
		return size;
	}

	public float [] getMeanColorComponents(int [] mPixels) {
		int aux_colour;

		float count = 0;
		int rh = 0;  int gs = 0;  int bb = 0;
		rgb_or_hsb[0] = 0.f; rgb_or_hsb[1] = 0.f; rgb_or_hsb[2] = 0.f;


		for(int p=0; p<npixels; p++) {

			aux_colour = mPixels[indexesOnOriginalImage[p]];

			if(DEBUG_SHOW_ANALYSIS_PIXELS){
				mPixels[indexesOnOriginalImage[p]] = 0;
			}

			rh += aux_colour >> 16 & 0xFF;	//	at this point this is r
			gs += aux_colour >> 8 & 0xFF;	//	at this point this is g	
			bb += aux_colour & 0xFF;//	at this point this is g


			count++;
		}

		rh/=count;  gs/=count;  bb/=count;
		if(currentColorMode == PConstants.RGB)
		{
			rgb_or_hsb[0] = rh;//*255;
			rgb_or_hsb[1] = gs;//*255;
			rgb_or_hsb[2] = bb;//*255;
		}
		else
		{
			rgb_or_hsb = Color.RGBtoHSB(rh, gs, bb, null);
			rgb_or_hsb[0] *= 255;
			rgb_or_hsb[1] *= 255;
			rgb_or_hsb[2] *= 255;
		}


		return rgb_or_hsb;
	}

	public boolean IsInsideSector(float X, float Y)
	{		
		return ( X > center.x - size.x/2 && X < center.x + size.x/2
		&& Y > center.y - size.y/2 && Y < center.y + size.y/2 );
	}

	//  Alterna estado (ligado <-> desligado)
	//  Alternates on/off state
	public void switchOnOffState() {
		isON = !isON;
	}
	
	public void setIsON(boolean on)
	{
		isON = on;
	}

	void displayOFF()
	{
		//  a square in its center
		processing.rectMode(PApplet.CENTER);
		processing.stroke(0);
		processing.noFill();
		processing.rect(center.x, center.y, 4, 4);
		//		processing.ellipse(center[0], center[1], 4, 4);
	}

	void display(int a_color)
	{
		if(!isON)
		{
			displayOFF();
			return;
		}

		//processing.tint(a_color); //WHY?

		//  paint entire sector
		processing.rectMode(PApplet.CENTER);
		processing.noStroke();
		processing.fill(a_color);
		processing.rect(center.x, center.y, size.x, size.y);
		//processing.tint(255); //WHY?

	}

	void display(int a_color, Point<Integer> pos)
	{
		if(!isON)
		{
			displayOFF();
			return;
		}

		//  paint entire sector
		processing.rectMode(PApplet.CENTER);
		processing.noStroke();
		processing.fill(a_color);
		//processing.tint(a_color);//WHY?
		processing.rect(pos.x, pos.y, size.x, size.y);
		//processing.tint(255); //WHY?
	}
	
	public static int getRows(){
		return AnalysisSector.rows;
	}
	
	public static int getColumns(){
		return AnalysisSector.columns;
	}
	
	public static void updateAccumulation(boolean[] found){
		updateAccumulation(found, DEFAULT_MAX_ACCUMULATION, DEFAULT_ACCUMULATION_FRACTION);
	}
	
	//	TODO  int maxAccumulation, int accumulationStep should be attributes!! 
	public static void updateAccumulation(boolean[] found, byte maxAccumulation, int accumulationFraction){
		for (int i = 0; i < found.length; i++){
			if (found[i] && AnalysisSector.accumulations[i] < maxAccumulation){
				AnalysisSector.accumulations[i] += maxAccumulation/accumulationFraction;//++
				if (AnalysisSector.accumulations[i] > maxAccumulation){
					AnalysisSector.accumulations[i] = maxAccumulation;
				}
			} else if (AnalysisSector.accumulations[i] > 0){
				AnalysisSector.accumulations[i]--;
			}
		}
	}
}
