package sectors;

public class MatrixSize {	
	
	public int rows;
	public int columns;
	   
	public MatrixSize (int rows, int columns){
		this.rows = rows;
		this.columns = columns;
	}
}
