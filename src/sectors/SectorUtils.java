package sectors;

import java.awt.Color;
import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PConstants;	//	for RGB

public final class SectorUtils {
	
	public static final float DEFAULT_THRESHOLD = 60f;
	
	public static AnalysisResult analyseSectors(int[] mPixels){
		return analyseSectors(mPixels, DEFAULT_THRESHOLD);
	}
	
	public static AnalysisResult analyseSectors(int[] mPixels, float threshold) {
		
		float [][] newColors = new float [AnalysisSector.sectors.length][3];
		boolean[] found = new boolean[AnalysisSector.sectors.length];
		
		int countFound = 0;
		Point<Integer> pos = new Point<Integer>(0,0);
		ArrayList<Point<Integer>> movingPoints = new ArrayList<Point<Integer>>();
		
		for (int as=0; as < (AnalysisSector.sectors.length); as++)
		{
			
			newColors[as] = AnalysisSector.sectors[as].getMeanColorComponents(mPixels);

			found[as] = hasColorChanged(newColors, AnalysisSector.lastFrameColors, threshold, as);
			
			AnalysisSector.states[as] = (byte) (found[as] ? 0x01 : 0x00 );
			
			if (found[as]){
				countFound++;
				pos.x += AnalysisSector.sectors[as].center.x;
				pos.y += AnalysisSector.sectors[as].center.y;
				movingPoints.add(AnalysisSector.sectors[as].center);
			}
			
			AnalysisSector.lastFrameColors[as][0] = newColors[as][0];
			AnalysisSector.lastFrameColors[as][1] = newColors[as][1];
			AnalysisSector.lastFrameColors[as][2] = newColors[as][2];
		}
		
		if (countFound > 0){
			pos.x /= countFound;
			pos.y /= countFound;
		}
		
		AnalysisResult result = new AnalysisResult(found, newColors, countFound, pos, movingPoints);		
		
		return result;
	}
	
	public static boolean hasColorChanged(
			float[][] currentColor, float[][] oldColor, float threshold, int sectorIndex)
	{
		return 	(Math.abs(currentColor[sectorIndex][0] - oldColor[sectorIndex][0]) > threshold) && 
				(Math.abs(currentColor[sectorIndex][1] - oldColor[sectorIndex][1]) > threshold) &&
				(Math.abs(currentColor[sectorIndex][2] - oldColor[sectorIndex][2]) > threshold);
	}
	
	public static void drawAllSectors(AnalysisSector[] mAnalysisSector, boolean[] found,
			float [][] colorToDraw, PApplet processing)
	{
		int numberOfSectors = AnalysisSector.rows * AnalysisSector.columns;
		
		for (int as = 0; as < numberOfSectors; as++){
			
			drawSector(mAnalysisSector, found, colorToDraw, AnalysisSector.rows, AnalysisSector.columns, 
					as, processing);
			
		}
	}
	
	public static void drawAllSectors(AnalysisSector[] mAnalysisSector, boolean[] found,
			int [] age, float [][] colorToDraw, PApplet processing)
	{
		int numberOfSectors = AnalysisSector.rows * AnalysisSector.columns;
		
		for (int as = 0; as < numberOfSectors; as++){
			
			if (age[as] > 0){
				
				int sectorToDisplay = as;
				
				if (AnalysisSector.mirror){
					sectorToDisplay = (AnalysisSector.columns -1 -(as % AnalysisSector.columns) 
							+ (AnalysisSector.columns)*((int)(as/AnalysisSector.columns)));
				}
				
				Point<Integer> pos = mAnalysisSector[sectorToDisplay].center;
				
				int mColor = processing.color(
						colorToDraw[as][0], 
						colorToDraw[as][1], 
						colorToDraw[as][2]);
				
				drawSector(mAnalysisSector[as], pos, mColor, processing);
			}
		}
	}
	
	public static void drawAllSectors(int colorStrong, int colorWeak, PApplet processing){
		drawAllSectors(AnalysisSector.DEFAULT_MAX_ACCUMULATION, colorStrong, colorWeak, processing);
	}
	
	public static void drawAllSectors(byte maxAccumulation, int colorStrong,
			int colorWeak, PApplet processing)
	{
		int numberOfSectors = AnalysisSector.rows * AnalysisSector.columns;
		float invertedMaxAccumulation = 1f / (int)maxAccumulation;

		for (int as = 0; as < numberOfSectors; as++)
		{
			if (AnalysisSector.accumulations[as] > 0)
			{
				int sectorToDisplay = as;
				if (AnalysisSector.mirror){
					sectorToDisplay = (AnalysisSector.columns -1 -(as % AnalysisSector.columns) 
							+ (AnalysisSector.columns)*((int)(as/AnalysisSector.columns)));
				}           
				Point<Integer> pos = AnalysisSector.sectors[sectorToDisplay].center;

				int a1 = colorStrong    >> 24   & 0xFF;
				int r1 = colorStrong    >> 16   & 0xFF;
				int g1 = colorStrong    >> 8    & 0xFF;
				int b1 = colorStrong            & 0xFF;

				int a2 = colorWeak      >> 24   & 0xFF;
				int r2 = colorWeak      >> 16   & 0xFF;
				int g2 = colorWeak      >> 8    & 0xFF;
				int b2 = colorWeak              & 0xFF;

				float accumulationFactor = invertedMaxAccumulation * AnalysisSector.accumulations[as];
				
				processing.pushStyle();
					processing.colorMode(PConstants.RGB);
				
					int mColor = processing.color(
						r2 + ((float) (r1 - r2)) * 9.f*accumulationFactor,
						g2 + ((float) (g1 - g2)) * 9.f*accumulationFactor, 
						b2 + ((float) (b1 - b2)) * 9.f*accumulationFactor,
						a2 + ((float) (a1 - a2)) * 9.f*accumulationFactor);
				processing.popStyle();
				
				drawSector(AnalysisSector.sectors[as], pos, mColor, processing); 
			}
		}

					
		/********			
		processing.pushStyle();
			processing.colorMode(PConstants.RGB);
			
			int a1 = colorStrong 	>> 24 	& 0xFF;
			int r1 = colorStrong 	>> 16 	& 0xFF;
			int g1 = colorStrong 	>> 8 	& 0xFF;
			int b1 = colorStrong 			& 0xFF;
			
			int a2 = colorWeak		>> 24 	& 0xFF;
			int r2 = colorWeak 		>> 16 	& 0xFF;
			int g2 = colorWeak 		>> 8 	& 0xFF;
			int b2 = colorWeak 				& 0xFF;
			
			int numberOfSectors = AnalysisSector.rows * AnalysisSector.columns;
			float invertedMaxAccumulation = 1f / maxAccumulation;
			
			for (int as = 0; as < numberOfSectors; as++){
				
				if (AnalysisSector.accumulation[as] > 0){
					
					int sectorToDisplay = as;
					
					if (AnalysisSector.mirror){
						sectorToDisplay = (AnalysisSector.columns -1 -(as % AnalysisSector.columns) 
								+ (AnalysisSector.columns)*((int)(as/AnalysisSector.columns)));
					}
					
					Point<Integer> pos = AnalysisSector.sectors[sectorToDisplay].center;
					
					float ageFactor = invertedMaxAccumulation * AnalysisSector.accumulation[as];
					
					int mColor = processing.color(
							r2 + ((float) (r1 - r2)) * ageFactor,
							g2 + ((float) (g1 - g2)) * ageFactor, 
							b2 + ((float) (b1 - b2)) * ageFactor,
							a2 + ((float) (a1 - a2)) * ageFactor
							);
									
					drawSector(AnalysisSector.sectors[as], pos, mColor, processing);
				}
			}
		processing.popStyle();**********/
	}
	
	public static void drawSector(AnalysisSector sectorToDraw, Point<Integer> position,  
			int colorToDraw, PApplet processing)
	{		
		sectorToDraw.display(colorToDraw, position);
	}
	
	public static void drawSector(AnalysisSector[] mAnalysisSector, boolean[] found, 
			float [][] colorToDraw, int rows, int columns, int as, PApplet processing)
	{
		
		int sectorToDisplay = as;
		
		if (AnalysisSector.mirror){
			sectorToDisplay = (columns -1 -(as % columns) 
					+ (columns)*((int)(as/columns)));
		}
		
		Point<Integer> pos = mAnalysisSector[sectorToDisplay].center;
		
		if (found[as]) {
			mAnalysisSector[sectorToDisplay]
					.display(processing.color(colorToDraw[as][0], 
							colorToDraw[as][1], 
							colorToDraw[as][2], 180), 
							pos);
		}		
	}
	
	public static void drawSector(AnalysisSector[] mAnalysisSector, boolean[] found, 
			int [] age, float [][] colorToDraw, int rows, int columns, int as, PApplet processing)
	{
		
		int sectorToDisplay = as;
		
		if (AnalysisSector.mirror){
			sectorToDisplay = (columns -1 -(as % columns) 
					+ (columns)*((int)(as/columns)));
		}
		
		Point<Integer> pos = mAnalysisSector[sectorToDisplay].center;
		
		if (age[as] > 0) {
			mAnalysisSector[sectorToDisplay]
					.display(processing.color(colorToDraw[as][0], 
							colorToDraw[as][1], 
							colorToDraw[as][2]), 
							pos);
		}	
	}
	
	public static void subtractBackground(){
		
	}
	
	public static int[] sectorToScreenPosition(
			int sectorIndex, int numberOfSectorRows, int numberOfSectorColumns,
			int width, int height){
		
		int [] sectorPosition	= new int[2];
		int [] screenPosition 	= new int[2];
		
		int sectorToDisplay = sectorIndex;
		
		if (AnalysisSector.mirror){
			sectorToDisplay = (numberOfSectorColumns -1 -(sectorIndex % numberOfSectorColumns) 
					+ (numberOfSectorColumns)*((int)(sectorIndex/numberOfSectorColumns)));
		}
		
		sectorPosition[0] = sectorToDisplay / numberOfSectorColumns;
		sectorPosition[1] = sectorToDisplay - sectorPosition[0] * numberOfSectorColumns;
		
		// get the center of analysisSector, w/o the analysisSector array
		screenPosition[0] = sectorPosition[1] * width / numberOfSectorColumns 
				+ width / numberOfSectorColumns / 2;
		screenPosition[1] = sectorPosition[0] * height / numberOfSectorRows 
				+ height / numberOfSectorRows / 2;
		
		return screenPosition; 
	}
	
	public static int [] getAveragePositionOfChangingSectors
	(AnalysisSector[] mSectors, boolean[] found){
		
		int[] changingData = new int[3];
		
		// position X
		changingData[0] = 0;
		
		// position Y
		changingData[1] = 0;
		
		// weight: number of changing sectors
		changingData[2] = 0;
		
		
		// Check if found[] have the same length as mSectors
		if (found.length == mSectors.length){		
			for (int i=0; i<found.length;i++){
				if (found[i]){
					changingData[0] += mSectors[i].center.x;
					changingData[1] += mSectors[i].center.y;
					changingData[2]++;
				}
			}
			
			if (changingData[2] > 0){
				changingData[0] /= changingData[2];
				changingData[1] /= changingData[2];
			}
			
			return changingData;
		} else {
			System.out.println("Parameter error: found[] and mSectors should have the same length");
		}
		
		return null;
		
	}
}
