package sectors;

//import processing.core.PGraphics;
import processing.core.PApplet;
import main.SoMoConfig;

public final class SectorBuilder {
	
	
	public static void createSectors(
			Point<Integer> screenSize, 
			MatrixSize sizeRC,
			int colorMode,
			PApplet processing) //PGraphics canvas
	{
		createSectors(screenSize.x, screenSize.y, sizeRC.rows, sizeRC.columns, colorMode, processing);
	}
	
	public static void createSectors(SoMoConfig config) 
	{
		createSectors(config.screenSize.x, config.screenSize.y, config.sectorsMatrixSize.rows, config.sectorsMatrixSize.columns, 
				config.skippingPixels.x, config.skippingPixels.y, config.colorMode, config.processing);
	}
	
	public static void createSectors(
			Point<Integer> screenSize, 
			MatrixSize sizeRC, 
			Point<Integer> skippingPixels,
			int colorMode,
			PApplet processing) //PGraphics canvas
	{
		createSectors(screenSize.x, screenSize.y, sizeRC.rows, sizeRC.columns, 
				skippingPixels.x, skippingPixels.y, colorMode, processing);
	}
	
	public static void createSectors(int width, int height, int rows, int columns, int colorMode, PApplet processing) {
				
		Point<Integer> screenSize = new Point<>(width, height);
		MatrixSize sizeRC = new MatrixSize(rows, columns);
		
		AnalysisSector.rows 	= rows;
		AnalysisSector.columns 	= columns;
		AnalysisSector.accumulations = new byte[rows*columns];
		AnalysisSector.sectors = new AnalysisSector[rows*columns];
		AnalysisSector.states = new byte[rows*columns];
		AnalysisSector.lastFrameColors = new float[rows*columns][3];
		AnalysisSector.currentFrameColors = new float[rows*columns][3];
		
		for (int i = 0; i<rows; i++)  //  lines
		{
			for (int j = 0; j<columns; j++)  //  columns
			{
				
				Point<Integer> currentSector = new Point<>(i, j);
				Point<Integer> sectorCenter = getCenterPointOfASector(currentSector, screenSize, sizeRC);
				Point<Integer> sectorSize = getSectorSize(screenSize, sizeRC);
				Point<Integer> jump = getJump(sectorSize);
				
				int sectorFlatIndex = getFlatIndex(i, j, columns);
				
				AnalysisSector.sectors[sectorFlatIndex] = 
					new AnalysisSector(sectorCenter, sectorSize, sectorSize, jump, colorMode, processing);
			}
		}
	}
	
	public static void createSectors(int width, int height, int rows, int columns, 
			int jumpX, int jumpY, int colorMode, PApplet processing) {
		
		Point<Integer> screenSize = new Point<>(width, height);
		MatrixSize sizeRC = new MatrixSize(rows, columns);
		Point<Integer> jump = new Point<Integer>(jumpX, jumpY);
		
		AnalysisSector.rows 	= rows;
		AnalysisSector.columns 	= columns;
		AnalysisSector.accumulations = new byte[rows*columns];
		AnalysisSector.states = new byte[rows*columns];
		AnalysisSector.sectors = new AnalysisSector[rows*columns];
		AnalysisSector.lastFrameColors = new float[rows*columns][3];
		AnalysisSector.currentFrameColors = new float[rows*columns][3];
		
		for (int i = 0; i<rows; i++)  //  lines
		{
			for (int j = 0; j<columns; j++)  //  columns
			{
				
				Point<Integer> currentSector = new Point<>(i, j);
				Point<Integer> sectorCenter = getCenterPointOfASector(currentSector, screenSize, sizeRC);
				Point<Integer> sectorSize = getSectorSize(screenSize, sizeRC);
				
				int sectorFlatIndex = getFlatIndex(i, j, columns);
				
				AnalysisSector.sectors[sectorFlatIndex] = 
					new AnalysisSector(sectorCenter, sectorSize, sectorSize, jump, colorMode, processing);
			}
		}
	}
	
	private static int getFlatIndex(int i, int j, int columns){
		return columns * i + j;
	}
	
	private static Point<Integer> getCenterPointOfASector(int i, int j, int width, int height, int rows, int columns){
		Point<Integer> point = new Point<Integer>(j*width/columns + width/columns/2, i*height/rows + height/rows/2);
		return point;
	}
	
	private static Point<Integer> getCenterPointOfASector(Point<Integer> currentSector, Point<Integer> screenSize, MatrixSize sizeRC){
		Point<Integer> point = getCenterPointOfASector(currentSector.x, currentSector.y, screenSize.x, screenSize.y, sizeRC.rows, sizeRC.columns);
		return point;
	}
	
	private static Point<Integer> getSectorSize(Point<Integer> screenSize, MatrixSize sizeRC){
		return new Point<Integer>(screenSize.x/sizeRC.columns, screenSize.y/sizeRC.rows);
	}
	
	private static Point<Integer> getJump(Point<Integer> sectorSize){
		
		Point<Integer> analysisSize = new Point<Integer>(
				Math.max(1, sectorSize.x/8),
				Math.max(1, sectorSize.y/8)
			);
		
		return analysisSize; 
	}
}
