package sectors;

import java.util.ArrayList;

public class AnalysisResult {
	public boolean[] found;
	public float[][] newColors; 
	public int foundCount;
	public Point<Integer> pos;
	public ArrayList<Point<Integer>> movingPoints;
	 
	AnalysisResult(boolean[] found, float[][] newColors, int countFound,
			Point<Integer> pos, ArrayList<Point<Integer>> movingPoints) 
	{
		this.found = found;
		this.newColors = newColors;
		this.foundCount = countFound;
		this.pos = pos;
		this.movingPoints = movingPoints;
	}
}
