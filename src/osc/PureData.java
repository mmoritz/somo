package osc;

import java.util.ArrayList;
import java.util.List;
import osc.OscCom;

import body.BodyManager;
import main.Definitions;
import sectors.AnalysisSector;
import sectors.Point;

public class PureData {
	
	public float centerX = -1f;	
	public float width = -1;
	
	//	Parameters to be send to the synth automatically (the ongoing result of the analysis of movement)
	public float pitchChangePeriod;
	public float clipperLevel;
	public float volumeLevel;
	
	float superiorScreenLimit = -1f;
	float inferiorScreenLimit = -1f;	
	//float mMaximumVolumeChange = 0.002f;
	//float mMaximumClipperChange = 5.f;
	//float mLastVolumeLevel;
	//float deltaVolume;
	//float mLastClipperLevel;
	//float deltaClipper;
	
	//	Parameters to manually influence the synth
	public int   uivoON          = 0;
	public int   clipperON       = 0;
	public float levelMultiplier = 1.f;
	
	String messageName;	//	we're going to send all instrument's parameters in this same message
	String messageName_manual;	//	this is for the manual changes messages
	
	OscCom osc;
	
	
	public PureData(OscCom osc, String a_messageName, String a_messageNameManual)
	{
		this.centerX = -1f;
		this.width = -1f;
		this.osc = osc;
		this.messageName = a_messageName;
		this.messageName_manual = a_messageNameManual;
	}
	
	public void update(BodyManager bodyManager, Point<Integer> screenSize, int foundCount) {
		if(bodyManager.mBodies.size() > 0)
		{
			this.superiorScreenLimit = 1.0f - bodyManager.getMinY()/screenSize.y;
			updatePeriod(superiorScreenLimit);
			this.centerX = bodyManager.getCentroidOfAllBodies().x/screenSize.x;
			this.width = bodyManager.reachX()/screenSize.x;
			float inferiorScreenLimit = bodyManager.getMaxY()/screenSize.y;
			
			updateClipperLevel(inferiorScreenLimit);
			//mLastClipperLevel = clipperLevel;
			
			//handleAbruptVariations(foundCount);	Now handled in the synth
			volumeLevel = 8.0f*foundCount/((float)(AnalysisSector.rows * AnalysisSector.columns));
			assureLevelGreatThanResidual();
			//mLastVolumeLevel = volumeLevel;
			
		}
		else {
			rampToResidual();
			//mLastVolumeLevel = volumeLevel;
		}
	}
	
	public void send(){
		List<Object> data = new ArrayList<Object>();
		data.add(pitchChangePeriod);
		data.add(centerX);				
		data.add(width);
		data.add(clipperLevel);
		data.add(volumeLevel);
		
		osc.send(data, this.messageName);
	}
	
	public void send_manual()
	{
		List<Object> data = new ArrayList<Object>();
		data.add(uivoON);
		data.add(clipperON);
		data.add(levelMultiplier);
		osc.send(data, this.messageName_manual);
	}
	
	private void updatePeriod(float superiorScreenLimit)
	{
		this.pitchChangePeriod = isAboveHalfScreen(superiorScreenLimit) ? superiorScreenLimit : 0.5f;
	}
	
	private boolean isAboveHalfScreen(float superiorScreenLimit){
		return (superiorScreenLimit > 0.5f);
	}
	
	private void updateClipperLevel(float inferiorScreenLimit)
	{
		if(inferiorScreenLimit > 0.6f)
		{
			clipperLevel = 100.f * (inferiorScreenLimit - 0.6f)/0.4f;
			/*Now it's handled on the synth
			deltaClipper = clipperLevel - mLastClipperLevel;
			
			if(deltaClipper > mMaximumClipperChange)
			{
				clipperLevel = mLastClipperLevel + mMaximumClipperChange;
			}
			else if(deltaClipper < -mMaximumClipperChange)
			{
				clipperLevel = mLastClipperLevel - mMaximumClipperChange;
			}*/
		}
		else
		{
			clipperLevel = 0.f;
		}
	}
	
	private void assureLevelGreatThanResidual()
	{
		if(volumeLevel < Definitions.residualLevel) {
			volumeLevel = Definitions.residualLevel;
		}
	}
	
	/*Now it's handled on the synth
	private void handleAbruptVariations(int foundCount)
	{
		volumeLevel = 8.0f*foundCount/((float)(AnalysisSector.rows * AnalysisSector.columns));
				
		assureLevelGreatThanResidual();
	}*/
	
	private void rampToResidual()
	{
		volumeLevel -= 0.00016f;	
		assureLevelGreatThanResidual();
	}
}
