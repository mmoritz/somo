package osc;
import java.util.ArrayList;
import java.util.List;

import processing.core.PApplet;
import oscP5.*;
import netP5.*;

public class OscCom {
	PApplet processing;
	List<Object> data;
	NetAddress myRemoteLocation;
	OscP5 oscP5;
	
	
	public OscCom(PApplet processing, String ipAddress, int port)
	{
		this.processing 		= processing; 
		this.data				= new ArrayList<Object>();
		this.myRemoteLocation	= new NetAddress(ipAddress, port);
		this.oscP5				= new OscP5(this, port);
	}
	
	OscCom(PApplet processing, List<Object> data, String ipAddress, int port)
	{	
		this.processing 		= processing; 
		this.data				= data;
		this.myRemoteLocation	= new NetAddress(ipAddress, port);
		this.oscP5				= new OscP5(this, port);
	}
	
	public OscCom(PApplet processing, String ipAddress, int port, int datagramSize)
	{
		this.processing 		= processing; 
		this.data				= new ArrayList<Object>();
		this.myRemoteLocation	= new NetAddress(ipAddress, port);
		OscProperties properties= new OscProperties();
		properties.setDatagramSize(datagramSize);
		properties.setRemoteAddress(this.myRemoteLocation);
		this.oscP5				= new OscP5(this, properties);
	}
	
	OscCom(PApplet processing, List<Object> data, String ipAddress, int port, int datagramSize)
	{	
		this.processing 		= processing; 
		this.data				= data;
		this.myRemoteLocation	= new NetAddress(ipAddress, port);
		OscProperties properties= new OscProperties();
		properties.setDatagramSize(datagramSize);
		properties.setRemoteAddress(this.myRemoteLocation);
		this.oscP5				= new OscP5(this, properties);
	}
	
	void updateValues(List<Object> data){
		this.data = data;
	}
	
	void setNetworkAddress(NetAddress myRemoteLocation){
		this.myRemoteLocation = myRemoteLocation;
	}
	
	// TODO: provide support to other data types listed here:
	// http://www.sojamo.de/libraries/oscP5/reference/oscP5/OscMessage.html
	/**
	 * send OSC Message. Currently supports float, string and int data types
	 */
	public void send(String a_messageName){
		OscMessage myMessage = new OscMessage(a_messageName);
		for (Object o : data){
			// TODO: use the pattern
			if (o.getClass().equals(Float.class)) {
				myMessage.add((float)o);
		    }
		    else if (o.getClass().equals(String.class)) {
		    	myMessage.add(o.toString());
		    }
		    else if (o.getClass().equals(Integer.class)) {
		    	myMessage.add((int)o);
		    }
		    else {
		    	PApplet.println("Error - type sent not implemented");
		    }		
		}
		
		oscP5.send(myMessage, myRemoteLocation);
	}
	
	public void send(List<Object> data, String a_messageName){
		updateValues(data);
		send(a_messageName);
	}
	
	public void send(float val, String a_messageName){
		this.data.clear();
		data.add(val);
		updateValues(data);
		send(a_messageName);
	}
	
	public void send(int val, String a_messageName){
		this.data.clear();
		data.add(val);
		updateValues(data);
		send(a_messageName);
	}
	
	public void sendChunk(byte [] values, String a_messageName){
		OscMessage myMessage = new OscMessage(a_messageName);
		myMessage.add(values);
		oscP5.send(myMessage, myRemoteLocation);
	}
	
	public void sendChunk(int [] values, String a_messageName){
		OscMessage myMessage = new OscMessage(a_messageName);
		myMessage.add(values);
		oscP5.send(myMessage, myRemoteLocation);
	}
}