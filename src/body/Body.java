package body;

import megamu.mesh.Hull;
import megamu.mesh.MPolygon;
import processing.core.PApplet;
import processing.core.PVector;

public class Body {
	
	//static final int BODY_AGE_TO_LAST = 5; moved to Definitions.java
	
	float[][] bodyPoints;
	MPolygon bodyPolygon;
	int bodyColor;
	int bodyAge;
	PApplet processing;
	final int defaultColor = 0x10FF0000;	//	transparent red
	
	Body(float[][] bodyPoints, int bodyDisplayColor, PApplet processing){
		bodyCommonConstructor(bodyPoints, processing);
		this.bodyColor = bodyDisplayColor;
	}
	
	Body(float[][] bodyPoints, PApplet processing){
		bodyCommonConstructor(bodyPoints, processing);
		this.bodyColor = processing.color(255,255,255,50);
	}
	
	void bodyCommonConstructor(float[][] bodyPoints, PApplet processing){
		this.bodyPoints = bodyPoints;
		Hull myHull= new Hull(this.bodyPoints);
		bodyPolygon = myHull.getRegion();
		this.bodyAge = 0;
		this.processing = processing;
		
	}
	
	void setBodyColor(int bodyColor){
		this.bodyColor = bodyColor;
	}
	
	int getBodyAge(){
		return this.bodyAge;
	}
	
	public MPolygon getBodyPolygon(){
		return bodyPolygon;
	}
	
	void updateBody(){
		bodyAge++;
	}
	
	void displayBody(){
		processing.pushStyle();
			processing.noStroke();
			processing.fill(this.bodyColor);
			bodyPolygon.draw(processing);
		processing.popStyle();
	}
	
	PVector getCentroid(){
		return BodyManager.getCentroid(this.bodyPolygon);
	}
	
	float getMaxY(){
		return BodyManager.getMaxY(this.bodyPolygon);
	}
	
	float getMinY(){
		return BodyManager.getMinY(this.bodyPolygon);
	}
	
	PVector getMinMaxX(){
		return BodyManager.getMinMaxX(this.bodyPolygon);
	}		 
}