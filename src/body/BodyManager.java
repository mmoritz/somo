package body;
import java.util.ArrayList;

import main.Definitions;
import main.JavaML;
import main.SoMoConfig;
import main.VideoUtils;
import megamu.mesh.MPolygon;
import processing.core.PApplet;
import processing.core.PVector;
import processing.data.IntList;
import sectors.AnalysisSector;
import sectors.Point;

public class BodyManager {
		
	public final String TAG = BodyManager.this.getClass().getSimpleName();	
	
	PApplet processing;
	int width, height, numberOfSectors, maxFlows = 1;
	boolean showBodyPolygons=false, createFlows=false;
	
	
	public ArrayList<Body> mBodies;
	ArrayList<VideoButton> mVideoButtons;
	ArrayList<Flow> mFlows;
	float[][] primitivePoints;
	
	
	// maybe should be local
	ArrayList<float[][]> mDataSet;
	
	public BodyManager(SoMoConfig config){
		bodyManagerCommonConstructor(config.processing, config.screenSize.x, config.screenSize.y, config.sectorsCount, config.displayBodyPolygons);
	}
	
	public BodyManager(PApplet processing, Point<Integer> screenSize, int numberOfSectors, int displayPolygons){
		bodyManagerCommonConstructor(processing, screenSize.x, screenSize.y, numberOfSectors, displayPolygons);
	}
	
	public BodyManager(PApplet processing, int width, int height, int numberOfSectors, int displayPolygons){
		
		bodyManagerCommonConstructor(processing, width, height, numberOfSectors, displayPolygons);
	}
	
	void bodyManagerCommonConstructor(PApplet processing, int width, int height, int numberOfSectors, int displayPolygons){
		this.processing 		= processing;
		this.width				= width;
		this.height				= height;
		this.numberOfSectors  	= numberOfSectors;
			
		if(displayPolygons == 0) {
			this.showBodyPolygons = false;
		} else {
			this.showBodyPolygons = true;
		}
				
		
		mBodies 				= new ArrayList<Body>();
		mVideoButtons 			= new ArrayList<VideoButton>();
		mFlows					= new ArrayList<Flow>();		
	}
	
	public void setShowBodyPolygons(boolean showBodyPolygons) {
		this.showBodyPolygons = showBodyPolygons;
	}
	
	public boolean getShowBodyPolygons() {
		return this.showBodyPolygons;
	}
	
	public void setCreateFlows(boolean createFlows){
		this.createFlows = createFlows;
	}
	
	public void setMaxFlows(int maxFlows){
		this.maxFlows = maxFlows;
	}
	
	public void updateBodies(ArrayList<Point<Integer>> points){
		
		float[][] floatArrayPoints = new float[points.size()][2]; 
		
		for (int i=0;i<points.size();i++){
			floatArrayPoints[i][0] = points.get(i).x;
			floatArrayPoints[i][1] = points.get(i).y;
		}
		
		updateBodies(floatArrayPoints);
	}
	
	public void updateBodies(float[][] primitivePoints){
		
		this.primitivePoints = primitivePoints;
		
		if (AnalysisSector.mirror){
			for (int i=0; i<primitivePoints.length;i++){
				this.primitivePoints[i][0] = width - primitivePoints[i][0]; 
			}
		}
		
		if (primitivePoints.length > (int) (0.3 * (numberOfSectors))){
			addBody2(this.primitivePoints);
		} else {		
			
			mDataSet = VideoUtils.getKMeansClusters(primitivePoints, 1);
			for (int i=0; i<mDataSet.size(); i++){
				if (mDataSet.get(i).length > 10){
					addBody2(mDataSet.get(i));
				}
			}
		}
	}
	
	public void updateBodiesWithJavaML(ArrayList<Point<Integer>> points){
		
		float[][] floatArrayPoints = new float[points.size()][2]; 
		
		for (int i=0;i<points.size();i++){
			floatArrayPoints[i][0] = points.get(i).x;
			floatArrayPoints[i][1] = points.get(i).y;
		}
		
		updateBodiesWithJavaML(floatArrayPoints);
	}
	
	public void updateBodiesWithJavaML(float[][] primitivePoints){
		
		this.primitivePoints = primitivePoints;
		
		if (AnalysisSector.mirror){
			for (int i=0; i<primitivePoints.length;i++){
				this.primitivePoints[i][0] = width - primitivePoints[i][0]; 
			}
		}
		
		if (primitivePoints.length > (int) (Definitions.sectors_minPortion_createBody*numberOfSectors)){ 
			addBody2(this.primitivePoints);
		} else {					
			mDataSet = JavaML.runKMeans(primitivePoints, 1);
			for (int i=0; i<mDataSet.size(); i++){
				if (mDataSet.get(i).length > Definitions.sectors_minPortion_force1Cluster*numberOfSectors){
					addBody2(mDataSet.get(i));
				}
			}
		}
	}
	
	public void display(){
	
		IntList indexToDestroy = new IntList();
		
		for (int i=0;i<mBodies.size();i++){
			
			mBodies.get(i).updateBody();
						
			if (mBodies.get(i).getBodyAge() > Definitions.body_age_to_last){			
				indexToDestroy.append(i);
			}
			
			// Display
			if (mBodies.get(i).getBodyAge() <= Definitions.body_age_to_last)
			{
				if (showBodyPolygons) {
					mBodies.get(i).displayBody();
				}
			}
		}
		
		// Run backwards to avoid mess up with indexes
		for (int i=indexToDestroy.size()-1; i>-1;i--){
			mBodies.remove(indexToDestroy.get(i));
		}
		
		IntList flowIndexToDestroy = new IntList();
		
		if (createFlows){
			
			for (int i=0;i<mFlows.size();i++){

				if (mFlows.get(i).getFlowAge() > Flow.FLOW_AGE_TO_LAST){
					flowIndexToDestroy.append(i);
				}
				
				// Display flows
				mFlows.get(i).display();
			}
			
			// Run backwards to avoid mess up with indexes
			for (int i=flowIndexToDestroy.size()-1; i>-1;i--){
				mFlows.remove(flowIndexToDestroy.get(i));
			}
			
			if (mFlows.size() < maxFlows){
				generateFlow();
			}
		}
		
		// display VideoButtons
		for (VideoButton vb : mVideoButtons){
			vb.displayBody();
		}
	}
	
	void generateFlow(){
		
		if (mBodies.size() > 1){
			
			int ageStep = mBodies.get(mBodies.size()-1).getBodyAge() - mBodies.get(0).getBodyAge();
			
			PVector oldCentroid = mBodies.get(mBodies.size()-1).getCentroid();
			PVector newCentroid = mBodies.get(0).getCentroid();
			
			if (ageStep != 0){
				PVector speed = new PVector((newCentroid.x - oldCentroid.x)/ageStep, 
						(newCentroid.y - oldCentroid.y)/ageStep);
				
				float centroidDistance = PVector.dist(newCentroid, oldCentroid);
				MPolygon flowPolygon = mBodies.get(mBodies.size()-1).getBodyPolygon();
				
				
				if (centroidDistance > 60 && centroidDistance < 100){
					mFlows.add(new Flow(processing, flowPolygon, newCentroid, speed));
				}
			}
		}
	}
	
	@SuppressWarnings("unused")
	private void addBody(float[][] points) {
		Body mBody = null;
		
		try {
			mBody = new Body(points, processing);
		} catch (IllegalArgumentException e){
			System.err.println(TAG + " Illegal Argument" + pointsToString(points));
		} finally {
			if (mBody != null)
				mBodies.add(mBody);
		}
	}
	
	/**
	 * We add 2 points in a non-colinear way with the 1st point, ensuring that we have a non-colinear collection
	 * before passing to the Body constructor
	 */
	private void addBody2(float[][] points){
		
		float [][] points2 = new float[points.length + 2][2];
		System.arraycopy(points, 0, points2, 0, points.length);
		points2[points.length][0] = points[0][0] - 1;
		points2[points.length][1] = points[0][1] + 1;
		points2[points.length + 1][0] = points[0][0] + 1;
		points2[points.length + 1][1] = points[0][1] + 1;
		
		Body mBody = new Body(points2, processing);
		mBodies.add(mBody);		
	}
	
	
	
	
	
	
	// get the area of this polygon
	public static float getArea(MPolygon mPolygon){
		float _area = 0;

		for (int i=0; i<mPolygon.getCoords().length-1;i++){
			if (i<mPolygon.getCoords().length-1){
				_area += mPolygon.getCoords()[i][0]*mPolygon.getCoords()[i+1][1]; 
				_area -= mPolygon.getCoords()[i+1][0]*mPolygon.getCoords()[i][1];
			}
		}
		return PApplet.abs(_area/2);
	}



	// Get the centroid of this polygon
	public static PVector getCentroid(MPolygon mPolygon) {
		float sumX=0, sumY=0;  
		for (int i=0;i<mPolygon.getCoords().length;i++) {
			sumX += mPolygon.getCoords()[i][0];
			sumY += mPolygon.getCoords()[i][1];
		}
		return new PVector(sumX/mPolygon.getCoords().length, 
				sumY/mPolygon.getCoords().length);   
	}
	
	// Get the max value of a Y component of this polygon
	public static float getMaxY(MPolygon mPolygon) {
		float maxY=0;  
		for (int i=0;i<mPolygon.getCoords().length;i++) {
			maxY = Math.max(maxY, mPolygon.getCoords()[i][1]);
		}
		return maxY;
	}
	
	// Get the min value of a Y component of this polygon
	public static float getMinY(MPolygon mPolygon) {
		float minY = mPolygon.getCoords()[0][1];  
		for (int i=0;i<mPolygon.getCoords().length;i++) {
			minY = Math.min(minY, mPolygon.getCoords()[i][1]);
		}
		return minY;
	}
	
	// Get the reach of moving points (max value - min value) of the X component 
	public static PVector getMinMaxX(MPolygon mPolygon) {
		float minX=99999999;
		float maxX=0;
		
		for (int i=0;i<mPolygon.getCoords().length;i++) {
			minX = Math.min(minX, mPolygon.getCoords()[i][0]);
			maxX = Math.max(maxX, mPolygon.getCoords()[i][0]);
		}
		return new PVector(minX, maxX);
	}
	
	/**
	 * Get the maximum value of Y component between all the bodies
	 * @return maximum value of the coordinate Y (lowest screen position)
	 */
	public float getMaxY(){
		float maxY = 0;
		for (int i=0;i<this.mBodies.size();i++){
			float bodyMaxY = this.mBodies.get(i).getMaxY();
			maxY = Math.max(maxY, bodyMaxY);
		}
		return maxY;
	}
	
	/**
	 * Get the minimum value of Y component between all the bodies. 
	 * WARNING: The upper pixel on the screen has value zero.  
	 * @return minimum value of the coordinate Y (highest screen position) 
	 */
	public float getMinY(){
		float minY = height;
		for (int i=0;i<this.mBodies.size();i++){
			float bodyMinY = this.mBodies.get(i).getMinY();
			minY = Math.min(minY, bodyMinY);
		}
		return minY;
	}
	
	/**
	 * Get the max X distance between two points considering all bodies
	 * @return the value of (maxX - minX)
	 */
	public float reachX(){
		
		float minX = 99999999;
		float maxX = 0;
		
		for (int i=0;i<this.mBodies.size();i++){
			PVector minMax = this.mBodies.get(i).getMinMaxX();
			minX = Math.min(minX, minMax.x);
			maxX = Math.max(maxX, minMax.y);
		}
		
		return maxX-minX;
	}
	
	/**
	 * Get the mean position of all existing bodies.
	 * @return the position of the centroid.
	 */
	public PVector getCentroidOfAllBodies(){
		
		PVector centroid = new PVector(0,0);
		
		for (int i=0;i<this.mBodies.size();i++){
			PVector bodyCentroid = this.mBodies.get(i).getCentroid();
			centroid.x += bodyCentroid.x;
			centroid.y += bodyCentroid.y;
		}
		
		if (mBodies.size() > 0){
			centroid.x /= mBodies.size();
			centroid.y /= mBodies.size();
		}
		
		return centroid;
	}
	
	/**
	 * Verify whether a given point with coordinates [x,y] is inside of a video button or not 
	 * 
	 * @param testPoint
	 * @return - true if the point is inside any video button in mVideoButtons, false otherwise.
	 */
	public boolean isInsideAnyVideoButton(float[] testPoint) {
		for (VideoButton vb : mVideoButtons){
			if (vb.isInside(testPoint))
				return true;
		}
		return false;
	}
	
	
	
	/**
	 * Call the action of each activated Button
	 * 
	 */
	public void startActivatedButtonActions() {
		for (int i = 0; i < mVideoButtons.size(); i++){
			for (int j = 0; j < mBodies.size(); j++){
				if (poligonIntersection(mVideoButtons.get(i), mBodies.get(j))){
					if(!mVideoButtons.get(i).blocked)
						mVideoButtons.get(i).action();
				}
			}
		}
	}
	
	/**
	 * [DEPRECATED] Detect if some Body in mBodies is intersecting some VideoButton in mVideoButtons
	 * 
	 * @return the index of the activated video button in BodyManager.mVideoButtons; 
	 * return -1 if there is no intersection
	 * 
	 */
	public int getActivatedVideoButton() {
		for (int i = 0; i < mVideoButtons.size(); i++){
			for (int j = 0; j < mBodies.size(); j++){
				if (poligonIntersection(mVideoButtons.get(i), mBodies.get(j))){
					return i;
				}
			}
		}
		
		return -1;
	}
	
	/**
	 * To determine if a point "p" is inside a polygon, we draw a line from outside towards the point "p",
	 * if it cross some line of the polygon "n" times and "n" is an odd number, it is inside the polygon.
	 * 
	 * @param _myRegion
	 * @param testPoint
	 * @return
	 */
	public static boolean isInside(MPolygon _myRegion, float/*[]*/[] testPoint) {
		int countIntersections = 0;
		for (int i=0;i<_myRegion.count();i++) {
			//    text(Integer.toString(i),_myRegion.getCoords()[i][0], _myRegion.getCoords()[i][1]);
			if (i == _myRegion.count()-1) {
				if (segIntersection(
						new PVector(             -1, testPoint/*[0]*/[1]), 
						new PVector(testPoint/*[0]*/[0], testPoint/*[0]*/[1]), 
						new PVector(_myRegion.getCoords()[i][0], _myRegion.getCoords()[i][1]), 
						new PVector(_myRegion.getCoords()[0][0], _myRegion.getCoords()[0][1])))
				{
					countIntersections++;
				}
			}
			else {
				if (segIntersection(
						new PVector(             -1, testPoint/*[0]*/[1]), //added 0.1 to avoid touching some vertex 
						new PVector(testPoint/*[0]*/[0], testPoint/*[0]*/[1]), //added 0.1 to avoid touching some vertex
						new PVector( _myRegion.getCoords()[i][0], _myRegion.getCoords()[i][1]), 
						new PVector( _myRegion.getCoords()[i+1][0], _myRegion.getCoords()[i+1][1])))
				{
					countIntersections++;
				}
			}
		}
		return countIntersections%2 != 0;
	}
	
	/**
	 * Adapted from link above to return only true or false, 
	 * originally return the point of intersection
	 * http://wiki.processing.org/w/Line-Line_intersection
	 * 
	 * @param p1 - first point in first line
	 * @param p2 - second point in first line
	 * @param p3 - first point in second line
	 * @param p4 - second point in second line
	 * @return
	 */
	public static boolean segIntersection(PVector p1, PVector p2, PVector p3, PVector p4) { 
		float bx = p2.x - p1.x; 
		float by = p2.y - p1.y;
		float dx = p4.x - p3.x; 
		float dy = p4.y - p3.y;
		float b_dot_d_perp = bx * dy - by * dx;

		if (b_dot_d_perp == 0) { 
			return false;
		}
		float cx = p3.x - p1.x;
		float cy = p3.y - p1.y;
		float t = (cx * dy - cy * dx) / b_dot_d_perp;
		if (t < 0 || t > 1) {
			return false;
		}
		float u = (cx * by - cy * bx) / b_dot_d_perp;
		if (u < 0 || u > 1) { 
			return false;
		}
		return true;
	}
	
	/**
	 * Verify if a b1.bodyPolygon intersects b2.bodyPolygon. 
	 * 
	 * @param b1 - First body
	 * @param b2 - Second body
	 * @return true if they intersect each other, false otherwise.
	 */
	public static boolean poligonIntersection(Body b1, Body b2){
		MPolygon p1 = b1.bodyPolygon;
		MPolygon p2 = b2.bodyPolygon;
	
		for (int i = 0; i < p1.count(); i++){
			for (int j = 0; j < p2.count(); j++) {				
				if (segIntersection(
						new PVector(p1.getCoords()[i][0], p1.getCoords()[i][1]), 
						new PVector (p1.getCoords()[(i+1)%p1.count()][0], p1.getCoords()[(i+1)%p1.count()][1]), 
						new PVector(p2.getCoords()[j][0], p2.getCoords()[j][1]), 
						new PVector (p2.getCoords()[(j+1)%p2.count()][0], p2.getCoords()[(j+1)%p2.count()][1]))) {					
					return true;
				}
			}
		}
		return false;
	}
	
	public static String pointsToString(float[][] points){
		
		String pointsToDisplay = "{";
		
		for (int i = 0; i<points.length; i++){
			pointsToDisplay += "{";
			for (int j = 0; j<points[i].length; j++){
				pointsToDisplay += Float.toString(points[i][j]) + "f" + (j<(points[i].length-1) ? "," : "");  
			}
			pointsToDisplay += "}" + (i<(points.length-1) ? ", " : "");
		}
		
		pointsToDisplay += "}";
		
		return pointsToDisplay;
	}
	
	public void setScreenSize(int height, int width){
		this.height = height;
		this.width = width;
	}
}