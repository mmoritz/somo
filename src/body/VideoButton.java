package body;

import java.util.Timer;
import java.util.TimerTask;

import processing.core.PApplet;

abstract class VideoButton extends Body implements ActionButton {
	boolean blocked = false;
	Timer timer;
	long timerTime;
	
	public final long STANDARD_TIMER_TIME = 500;  
	
	/**
	 * 
	 * @param buttonPoints - center points of the button
	 * @param buttonDisplayColor - color of the button
	 * @param timerTime - minimum time between button activations, set zero for continuous action
	 */
	VideoButton(float[][] buttonPoints, int buttonDisplayColor, long timerTime, PApplet processing){
		super(buttonPoints, buttonDisplayColor, processing);
		this.timerTime = timerTime;
	}
	
	/**
	 * 
	 * @param buttonPoints - center points of the button
	 * @param timerTime - minimum time between button activations, set zero for continuous action
	 */
	VideoButton(float[][] buttonPoints, long timerTime, PApplet processing){
		super(buttonPoints, processing);
		this.timerTime = timerTime;
	}
	
	/**
	 * 
	 * @param buttonPoints - center points of the button
	 * @param buttonDisplayColor - color of the button
	 */
	VideoButton(float[][] buttonPoints, int buttonDisplayColor, PApplet processing){
		super(buttonPoints, buttonDisplayColor, processing);
		this.timerTime = STANDARD_TIMER_TIME;
	}
	
	/**
	 * 
	 * @param buttonPoints - center points of the button
	 */
	VideoButton(float[][] bodyPoints, PApplet processing) {
		super(bodyPoints, processing);
		this.timerTime = STANDARD_TIMER_TIME;
	}
	
	/**
	 * Template method to be implemented for each VideoButton.
	 * Specifies the action to be executed when the button is activated.
	 * Will be blocked by timerTime milliseconds after one activation.
	 */
	public void action(){
		if (blocked) {
			return;
		}
		blocked = true;
		timedUnblockAfterMiliseconds(timerTime);
	}

	/**
	 * Displays the button on the screen
	 */
	void displayBody() {
		if (!blocked)
			super.displayBody();
	}

	/**
	 * Informs if a given point is inside the Button
	 * 
	 * @param testPoint 
	 * @return true if the isInside, false otherwise
	 */
	boolean isInside(float[] testPoint) {
		return BodyManager.isInside(this.bodyPolygon, testPoint);
	}
	
	/**
	 * Adjust the minimum time between two button activations.  
	 * @param timerTime - time in milliseconds
	 */
	public void setTimerTime(long timerTime){
		this.timerTime = timerTime;
	}
	
	/**
	 * 
	 * @return - the minimum time between two button activations in milliseconds
	 */
	public long getTimerTime() {
		return this.timerTime;
	}
	
	/**
	 * Starts a Thread to a timer that unblocks the button after a given time in milliseconds.
	 * Implemented according this link:
	 * http://www.dsc.ufcg.edu.br/~jacques/cursos/map/html/threads/timer.html 
	 * @param miliseconds - time keep the button blocked.
	 */
	void timedUnblockAfterMiliseconds(long milliseconds){
		if (milliseconds > 0){				
			this.timer = new Timer();
			timer.schedule(new ButtonBlock(), milliseconds);
		} else {
			blocked = false;
		}
		
	}
	
	/**
	 * Defines a TimerTask to unblocks the button and stop the timer's Thread.
	 * Implemented based on this link: 
	 * http://www.dsc.ufcg.edu.br/~jacques/cursos/map/html/threads/timer.html
	 *
	 */
	class ButtonBlock extends TimerTask{
		public void run () {
//			System.out.println("timer " + timerTime);
			blocked = false;
			timer.cancel();
		}
	}
}