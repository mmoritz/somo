package body;

import megamu.mesh.MPolygon;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PVector;

class Flow {
	
	static final int FLOW_AGE_TO_LAST = 60; 
	
	PApplet processing;
	MPolygon polygon;
	PVector position;
	PVector speed;
	PGraphics gPolygon;
	int flowAge;
	PImage objectToLaunch;
	
	Flow(PApplet processing, MPolygon polygon, PVector position, PVector speed){
		this.processing = processing;
		this.polygon 	= polygon;
		this.position	= position;
		this.speed 		= speed;
		
		
		// Using PGraphics I don't need the position
		this.position 	= new PVector(0,0);
		gPolygon = processing.createGraphics(640,480); // FIXME: hardcoded
		gPolygon.beginDraw();
		gPolygon.fill(255,50,0,128);
		polygon.draw(this.gPolygon);
		gPolygon.endDraw();
		
		
		this.flowAge = 0;
		objectToLaunch = processing.loadImage("red.png");
	}
	
	void display(){
		// update age & position
		this.flowAge++;
		position.x += speed.x;
		position.y += speed.y;
		
		processing.image(gPolygon, position.x, position.y);
//		processing.image(objectToLaunch, position.x, position.y);
		
		// debug data
//		System.out.println("flowAge: " + flowAge
//				+ ", pos(" + position.x + ", " + position.y + ")");
	}
	
	int getFlowAge(){
		return this.flowAge;
	}
}