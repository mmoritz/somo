///  - Visualizador remoto do SoMo -  
///
///  IMPORTANTE
///  - Os setores vem invertidos em X. Sao desinvertidos aqui
///  - Os poligonos nao vem invertidos, mas vem na escala da tela do servidor. Sao reescalados aqui
///
///  TECLAS DE ATALHO
///  0 - desenhamos as /sectorsAccum como retangulos
///  1 - desenhamos as /sectorsON_OFF como retangulos
///  2 - populamos os pixels com as /sectorsAccum
///  3 - populamos os pixels com as /sectorsON_OFF
///  p - poligonos ON/OFF
///  b - blur ON/OFF
///  mouse click - novas cores aleatorias 
///
///  Tiago Brizolara, 2017/2018

import oscP5.*;
import netP5.*;

OscP5 oscP5;

int OPTION = 0;
boolean POLYGONS_ON = true;
boolean BLUR_ON = true;

byte [] lastSectorsAccumMessage;
byte [] lastSectorsON_OFFMessage;

//  Quantidade de setores (pixels grandoes) recebidos na horizontal e na vertical
final int INCOMING_SW = 160; //  HARD-CODED
final int INCOMING_SH = 96;  //  HARD-CODED

//  Largura e altura da captura do servidor
final int INCOMING_W = 320;  //  HARD-CODED
final int INCOMING_H = 240;  //  HARD-CODED
float scalePolygonsW, scalePolygonsH;
float scaleSectorsW, scaleSectorsH;
float sectorsW, sectorsH;

final int PORT = 8079; //  HARD-CODED

//  Bytes iniciais da mensagem OSC dos setores 
final int NON_IMAGE_BYTES = 24;

//  Copiando visualizacao original do SoMo
color colorStrong, colorWeak;
float maxAccumulation = 60.0f;
Object[] lastPolygonsMessage;
boolean polygonsLocked;

void setup()
{
  //size(800, 480, P2D);
  size(160, 96, P2D);
  
  updatePolygonsScale();
  sectorsW = width/float(INCOMING_SW);
  sectorsH = height/float(INCOMING_SH);
  
  colorMode(HSB, 255, 255, 255, 255);
  
  lastSectorsAccumMessage  = new byte[INCOMING_SH*INCOMING_SW*2];
  lastSectorsON_OFFMessage = new byte[INCOMING_SH*INCOMING_SW*2];
  
  OscProperties properties = new OscProperties();
  //  NAO SEI por que precisamos tantos bytes a mais que w*h...
  properties.setDatagramSize(INCOMING_SW*INCOMING_SH*2);
  properties.setListeningPort(PORT);
  NetAddress myRemoteLocation  = new NetAddress("localhost", 10000);
  properties.setRemoteAddress(myRemoteLocation);
  this.oscP5 = new OscP5(this, properties);
  
  background(0); 
  noStroke();
  
  lastPolygonsMessage = new Object[INCOMING_SH*INCOMING_SW*3];
  lastPolygonsMessage[0] = 0;
  polygonsLocked = false;
  
  frameRate(100);
  
  updateColors();
}

void draw()
{
  background(0);
  
  //  DESCOMENTE O TIPO DE DESENHO QUE QUER USAR
  
  switch(OPTION)
  {
    case 0: drawSectorsAccumulations(this.g); break; 
    case 1: drawSectorsON_OFF(this.g); break; 
    case 2: drawPixelsAccumulations(this.g); break;
    case 3: drawPixelsON_OFF(this.g); break;
  }
  
  //  Blur calculado sem shader...
  if(BLUR_ON) {
    blur(5);
  }
  
  if(POLYGONS_ON) {
    drawPolygons();
  }
}

//  Usa os dados vindos de /sectorsAccum
void drawSectorsAccumulations(PGraphics g)
{
  for(int i=0; i<INCOMING_SH; i++) {
    for(int j=0; j<INCOMING_SW; j++) {
      if(lastSectorsAccumMessage[INCOMING_SW*i + j + NON_IMAGE_BYTES] == 0x00) {
        g.fill(0, 0, 0);//, 0);
      }else {
        g.fill(getAccumulationColor(lastSectorsAccumMessage[INCOMING_SW*i + j + NON_IMAGE_BYTES]));
      }
      //  preciosismo: dobramos o tamanho do retangulo
      g.rect(width - sectorsW*j, sectorsH*i, sectorsW*2, sectorsH*2);
    }
  }
}

//  Usa os dados vindos de /sectorsON_OFF
void drawSectorsON_OFF(PGraphics g)
{
  for(int i=0; i<INCOMING_SH; i++) {
    for(int j=0; j<INCOMING_SW; j++) {
      if(lastSectorsON_OFFMessage[INCOMING_SW*i + j + NON_IMAGE_BYTES] == 0x00) {
        g.fill(0);//, 150);
      }else {
        g.fill(255);//,150);
      }
      //  preciosismo: dobramos o tamanho do retangulo
      g.rect(width - sectorsW*j, sectorsH*i, sectorsW*2, sectorsH*2);
    }
  }
}

void drawPixelsAccumulations(PGraphics g)
{
  g.loadPixels();
  for(int i=0; i<INCOMING_SH; i++) {
    for(int j=0; j<INCOMING_SW; j++) {
      if(lastSectorsAccumMessage[i*INCOMING_SW + j + NON_IMAGE_BYTES] == 0x00) {
        g.pixels[i*width + g.width-1 - j] = color(0);
      } else {
        g.pixels[i*width + g.width-1 - j] = getAccumulationColor(lastSectorsAccumMessage[i*INCOMING_SW + j + NON_IMAGE_BYTES]);
      }
    }
  }
  g.updatePixels();
}

void drawPixelsON_OFF(PGraphics g)
{
  g.loadPixels();
  for(int i=0; i<INCOMING_SH; i++) {
    for(int j=0; j<INCOMING_SW; j++) {
      g.pixels[i*width + g.width-1 - j] = lastSectorsON_OFFMessage[i*INCOMING_SW + j + NON_IMAGE_BYTES] == 0x00 ? color(0) : color(255);
    }
  }
  g.updatePixels();
}

void blur(int radius)
{
  loadPixels();
      //post_processing.BlurFilters.shiftBlur2(pixels, screenBuf, width, height);
      //post_processing.BlurFilters.shiftBlur2(screenBuf, pixels, width, height);
      BlurFilters.klingemannBlur(pixels, radius, width, height);
  updatePixels();  
}

void keyPressed()
{
  switch(key)
  {
    case '0': OPTION = 0; break;
    case '1': OPTION = 1; break;
    case '2': OPTION = 2; break;
    case '3': OPTION = 3; break;
    case 'p': POLYGONS_ON = !POLYGONS_ON; break;
    case 'b': BLUR_ON = !BLUR_ON; break;
  }
  
  updatePolygonsScale();
}

void updatePolygonsScale()
{
  if(OPTION < 2) {
    scalePolygonsW = width/float(INCOMING_W);
    scalePolygonsH = height/float(INCOMING_H);
  }
  else {
    scalePolygonsW = INCOMING_SW/float(INCOMING_W);
    scalePolygonsH = INCOMING_SH/float(INCOMING_H);
  } 
}

void mousePressed()
{
  updateColors();
  println(frameRate);
}

void oscEvent(OscMessage theOscMessage)
{
  //println(theOscMessage);
  if(theOscMessage.checkAddrPattern("/sectorsON_OFF")==true)
  {
    lastSectorsON_OFFMessage = theOscMessage.getBytes();
  }
  else if(theOscMessage.checkAddrPattern("/sectorsAccum")==true)
  {
    lastSectorsAccumMessage = theOscMessage.getBytes();
  }
  else if(theOscMessage.checkAddrPattern("/polygons")==true)
  {
    if(!polygonsLocked)
    {
      arrayCopy(theOscMessage.arguments(), lastPolygonsMessage);
    }
  }
}

//  Atualiza Fill() com uma mistura de colorStrong e colorWeak
int getAccumulationColor(byte accumulation)
{
  int a1 = colorStrong    >> 24   & 0xFF;
  int r1 = colorStrong    >> 16   & 0xFF;
  int g1 = colorStrong    >> 8    & 0xFF;
  int b1 = colorStrong            & 0xFF;

  int a2 = colorWeak      >> 24   & 0xFF;
  int r2 = colorWeak      >> 16   & 0xFF;
  int g2 = colorWeak      >> 8    & 0xFF;
  int b2 = colorWeak              & 0xFF;

  float accumulationFactor = (int)accumulation / maxAccumulation;
  
  pushStyle();
    colorMode(PConstants.RGB);
  
    int mColor = color(
      r2 + ((float) (r1 - r2)) * 9.f*accumulationFactor,
      g2 + ((float) (g1 - g2)) * 9.f*accumulationFactor, 
      b2 + ((float) (b1 - b2)) * 9.f*accumulationFactor,
      a2 + ((float) (a1 - a2)) * 9.f*accumulationFactor);
  popStyle();
  
  return mColor;
}

//  Atualiza strongCoolr e weakColor com valores aleatorios
void updateColors()
{
    float opt = random(1);
    int hue;
    /*if(opt < 0.5f) {
      hue = (int)random(32, 130);    
    } else {
      hue = (int)random(208,250);
    }*/
    hue = (int)random(255);
    
    float alpha = /*random(175);*/100 + random(155);
    processing.core.PApplet.println("randomColor1: hue = " + hue + " | alpha = " + alpha);
    
    colorStrong = color(hue, 255, 255, alpha);
    
    opt = random(1);
    /*if(opt < 0.5f) {
      hue = (int)random(32, 130);    
    } else {
      hue = (int)random(208,250);
    }*/
    hue = (int)random(255);
    
    alpha = 50 + random(50);//random(90);
    processing.core.PApplet.println("randomColor2: hue = " + hue + " | alpha = " + alpha);
    
    colorWeak = color(hue, 255, 255, alpha);
}

void drawPolygons()
{ 
  polygonsLocked = true;
  fill(255,255,255,100);
  int msgDataCount = 0;
  
  int nbodies = (int)lastPolygonsMessage[msgDataCount];//.get(msgDataCount).intValue();
  msgDataCount++;
  int npts;
  for(int b=0; b<nbodies; b++)
  {
    npts = (int)lastPolygonsMessage[msgDataCount];//.get(msgDataCount).intValue();
    msgDataCount++;
    beginShape();
    for(int p=0; p<npts; p++)
    {
      vertex(
        (float)lastPolygonsMessage[msgDataCount+2*p]*scalePolygonsW,
        (float)lastPolygonsMessage[msgDataCount+2*p+1]*scalePolygonsH);
    }
    endShape();
    msgDataCount += npts*2;
  }

  polygonsLocked = false;
}