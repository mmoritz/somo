//package post_processing;

/**/static/**/ public class BlurFilters {
  //  From Job Leonard van der Zwan work: http://jobleonard.nl/fastFilters/
  // 030
  // 343
  // 030
  // 16 in total, >>4

  static public void shiftBlur2(int[] s, int[] t, int width, int height){ // source & target buffer
    int yOffset;

    for (int i = 1; i < (width-1); ++i){
      
      yOffset = width*(height-1);
      // top edge (minus corner pixels)
      t[i] = (((((s[i] & 0xFF) << 2) + 
        ((s[i+1] & 0xFF) + 
        (s[i-1] & 0xFF) + 
        (s[i + width] & 0xFF) + 
        (s[i + yOffset] & 0xFF)) * 3) >>> 4)  & 0xFF) +
        (((((s[i] & 0xFF00) << 2) + 
        ((s[i+1] & 0xFF00) + 
        (s[i-1] & 0xFF00) + 
        (s[i + width] & 0xFF00) + 
        (s[i + yOffset] & 0xFF00)) * 3) >>> 4)  & 0xFF00) +
        (((((s[i] & 0xFF0000) << 2) + 
        ((s[i+1] & 0xFF0000) + 
        (s[i-1] & 0xFF0000) + 
        (s[i + width] & 0xFF0000) + 
        (s[i + yOffset] & 0xFF0000)) * 3) >>> 4)  & 0xFF0000) +
        0xFF000000; //ignores transparency

      // bottom edge (minus corner pixels)
      t[i + yOffset] = (((((s[i + yOffset] & 0xFF) << 2) + 
        ((s[i - 1 + yOffset] & 0xFF) + 
        (s[i + 1 + yOffset] & 0xFF) +
        (s[i + yOffset - width] & 0xFF) +
        (s[i] & 0xFF)) * 3) >>> 4) & 0xFF) +
        (((((s[i + yOffset] & 0xFF00) << 2) + 
        ((s[i - 1 + yOffset] & 0xFF00) + 
        (s[i + 1 + yOffset] & 0xFF00) +
        (s[i + yOffset - width] & 0xFF00) +
        (s[i] & 0xFF00)) * 3) >>> 4) & 0xFF00) +
        (((((s[i + yOffset] & 0xFF0000) << 2) + 
        ((s[i - 1 + yOffset] & 0xFF0000) + 
        (s[i + 1 + yOffset] & 0xFF0000) +
        (s[i + yOffset - width] & 0xFF0000) +
        (s[i] & 0xFF0000)) * 3) >>> 4) & 0xFF0000) +
        0xFF000000;    
      
      // central square
      for (int j = 1; j < (height-1); ++j){
        yOffset = j*width;
        t[i + yOffset] = (((((s[i + yOffset] & 0xFF) << 2) +
          ((s[i + 1 + yOffset] & 0xFF) +
          (s[i - 1 + yOffset] & 0xFF) +
          (s[i + yOffset + width] & 0xFF) +
          (s[i + yOffset - width] & 0xFF)) * 3) >>> 4) & 0xFF) +
          (((((s[i + yOffset] & 0xFF00) << 2) +
          ((s[i + 1 + yOffset] & 0xFF00) +
          (s[i - 1 + yOffset] & 0xFF00) +
          (s[i + yOffset + width] & 0xFF00) +
          (s[i + yOffset - width] & 0xFF00)) * 3) >>> 4) & 0xFF00) +
          (((((s[i + yOffset] & 0xFF0000) << 2) +
          ((s[i + 1 + yOffset] & 0xFF0000) +
          (s[i - 1 + yOffset] & 0xFF0000) +
          (s[i + yOffset + width] & 0xFF0000) +
          (s[i + yOffset - width] & 0xFF0000)) * 3) >>> 4) & 0xFF0000) +
          0xFF000000;
      }
    }
    
    // left and right edge (minus corner pixels)
    for (int j = 1; j < (height-1); ++j){
        yOffset = j*width;
        t[yOffset] = (((((s[yOffset] & 0xFF) << 2) +
          ((s[yOffset + 1] & 0xFF) +
          (s[yOffset + width - 1] & 0xFF) +
          (s[yOffset + width] & 0xFF) +
          (s[yOffset - width] & 0xFF) ) * 3) >>> 4) & 0xFF) +
          (((((s[yOffset] & 0xFF00) << 2) +
          ((s[yOffset + 1] & 0xFF00) +
          (s[yOffset + width - 1] & 0xFF00) +
          (s[yOffset + width] & 0xFF00) +
          (s[yOffset - width] & 0xFF00) ) * 3) >>> 4) & 0xFF00) +
          (((((s[yOffset] & 0xFF0000) << 2) +
          ((s[yOffset + 1] & 0xFF0000) +
          (s[yOffset + width - 1] & 0xFF0000) +
          (s[yOffset + width] & 0xFF0000) +
          (s[yOffset - width] & 0xFF0000) ) * 3) >>> 4) & 0xFF0000) +
          0xFF000000;

        t[yOffset + width - 1] = (((((s[yOffset + width - 1] & 0xFF) << 2) +
          ((s[yOffset] & 0xFF) +
          (s[yOffset + width - 2] & 0xFF) +
          (s[yOffset + (width<<1) - 1] & 0xFF) +
          (s[yOffset - 1] & 0xFF)) * 3) >>> 4) & 0xFF) +
          (((((s[yOffset + width - 1] & 0xFF00) << 2) +
          ((s[yOffset] & 0xFF00) +
          (s[yOffset + width - 2] & 0xFF00) +
          (s[yOffset + (width<<1) - 1] & 0xFF00) +
          (s[yOffset - 1] & 0xFF00)) * 3) >>> 4) & 0xFF00) +
          (((((s[yOffset + width - 1] & 0xFF0000) << 2) +
          ((s[yOffset] & 0xFF0000) +
          (s[yOffset + width - 2] & 0xFF0000) +
          (s[yOffset + (width<<1) - 1] & 0xFF0000) +
          (s[yOffset - 1] & 0xFF0000)) * 3) >>> 4) & 0xFF0000) +
          0xFF000000;
    }
    
    // corner pixels
    t[0] = (((((s[0] & 0xFF) << 2) + 
      ((s[1] & 0xFF) + 
      (s[width-1] & 0xFF) + 
      (s[width] & 0xFF) + 
      (s[width*(height-1)] & 0xFF)) * 3) >>> 4)  & 0xFF) +
      (((((s[0] & 0xFF00) << 2) + 
      ((s[1] & 0xFF00) + 
      (s[width-1] & 0xFF00) + 
      (s[width] & 0xFF00) + 
      (s[width*(height-1)] & 0xFF00)) * 3) >>> 4)  & 0xFF00) +
      (((((s[0] & 0xFF0000) << 2) + 
      ((s[1] & 0xFF0000) + 
      (s[width-1] & 0xFF0000) + 
      (s[width] & 0xFF0000) + 
      (s[width*(height-1)] & 0xFF0000)) * 3) >>> 4)  & 0xFF0000) +
      0xFF000000;

    t[width - 1 ] = (((((s[width-1] & 0xFF) << 2) + 
      ((s[width-2] & 0xFF) + 
      (s[0] & 0xFF) + 
      (s[(width<<1) - 1] & 0xFF) + 
      (s[width*height-1] & 0xFF) ) * 3) >>> 4) & 0xFF) +
      (((((s[width-1] & 0xFF00) << 2) + 
      ((s[width-2] & 0xFF00) + 
      (s[0] & 0xFF00) + 
      (s[(width<<1) - 1] & 0xFF00) + 
      (s[width*height-1] & 0xFF00) ) * 3) >>> 4) & 0xFF00) +
      (((((s[width-1] & 0xFF0000) << 2) + 
      ((s[width-2] & 0xFF0000) + 
      (s[0] & 0xFF0000) + 
      (s[(width<<1) - 1] & 0xFF0000) + 
      (s[width*height-1] & 0xFF0000) ) * 3) >>> 4) & 0xFF0000) +
      0xFF000000;

    t[width * height - 1] = (((((s[width*height-1] & 0xFF) << 2) + 
      ((s[width-1] & 0xFF) + 
      (s[width*(height-1)-1] & 0xFF) + 
      (s[width*height-2] & 0xFF) + 
      (s[width*(height-1)] & 0xFF) ) * 3) >>> 4) & 0xFF) +
      (((((s[width*height-1] & 0xFF00) << 2) + 
      ((s[width-1] & 0xFF00) + 
      (s[width*(height-1)-1] & 0xFF00) + 
      (s[width*height-2] & 0xFF00) + 
      (s[width*(height-1)] & 0xFF00) ) * 3) >>> 4) & 0xFF00) +
      (((((s[width*height-1] & 0xFF0000) << 2) + 
      ((s[width-1] & 0xFF0000) + 
      (s[width*(height-1)-1] & 0xFF0000) + 
      (s[width*height-2] & 0xFF0000) + 
      (s[width*(height-1)] & 0xFF0000) ) * 3) >>> 4) & 0xFF0000) +
      0xFF000000;
    
    t[width *(height-1)] = (((((s[width*(height-1)] & 0xFF) << 2) + 
      ((s[width*(height-1) + 1] & 0xFF) + 
      (s[width*height-1] & 0xFF) + 
      (s[width*(height-2)] & 0xFF) + 
      (s[0] & 0xFF) ) * 3) >>> 4) & 0xFF) +
      (((((s[width*(height-1)] & 0xFF00) << 2) + 
      ((s[width*(height-1) + 1] & 0xFF00) + 
      (s[width*height-1] & 0xFF00) + 
      (s[width*(height-2)] & 0xFF00) + 
      (s[0] & 0xFF00) ) * 3) >>> 4) & 0xFF00) +
      (((((s[width*(height-1)] & 0xFF0000) << 2) + 
      ((s[width*(height-1) + 1] & 0xFF0000) + 
      (s[width*height-1] & 0xFF0000) + 
      (s[width*(height-2)] & 0xFF0000) + 
      (s[0] & 0xFF0000) ) * 3) >>> 4) & 0xFF0000) +
      0xFF000000;
  }

  static public int max(int a, int b) {
    return a > b ? a : b;
  }
  static public int min(int a, int b) {
    return a < b ? a : b;
  }
  
  // Modified version of Mario Klingemann's Super Fast Blur, for comparison

  // Super Fast Blur v1.1
  // by Mario Klingemann <http://incubator.quasimondo.com>
  //
  // Tip: Multiple invovations of this filter with a small
  // radius will approximate a gaussian blur quite well.

  static public void klingemannBlur(int [] pix, int radius, int w, int h) {
    if (radius<1) {
      return;
    }
    int wm=w-1;
    int hm=h-1;
    int wh=w*h;
    int div=radius+radius+1;
    int r[]=new int[wh];
    int g[]=new int[wh];
    int b[]=new int[wh];
    int rsum,gsum,bsum,x,y,i,p,p1,p2,yp,yi,yw;
    int vmin[] = new int[max(w,h)];
    int vmax[] = new int[max(w,h)];
    int dv[]=new int[256*div];
    for (i=0;i<256*div;i++) {
      dv[i]=(i/div);
    }
    yw=yi=0;
    for (y=0;y<h;y++) {
      rsum=gsum=bsum=0;
      for(i=-radius;i<=radius;i++) {
        p=pix[yi+min(wm,max(i,0))];
        rsum+=(p & 0xff0000)>>16;
        gsum+=(p & 0x00ff00)>>8;
        bsum+= p & 0x0000ff;
      }
      for (x=0;x<w;x++) {
        r[yi]=dv[rsum];
        g[yi]=dv[gsum];
        b[yi]=dv[bsum];
        if(y==0) {
          vmin[x]=min(x+radius+1,wm);
          vmax[x]=max(x-radius,0);
        }
        p1=pix[yw+vmin[x]];
        p2=pix[yw+vmax[x]];
        rsum+=((p1 & 0xff0000)-(p2 & 0xff0000))>>16;
        gsum+=((p1 & 0x00ff00)-(p2 & 0x00ff00))>>8;
        bsum+= (p1 & 0x0000ff)-(p2 & 0x0000ff);
        yi++;
      }
      yw+=w;
    }
    for (x=0;x<w;x++) {
      rsum=gsum=bsum=0;
      yp=-radius*w;
      for(i=-radius;i<=radius;i++) {
        yi=max(0,yp)+x;
        rsum+=r[yi];
        gsum+=g[yi];
        bsum+=b[yi];
        yp+=w;
      }
      yi=x;
      for (y=0;y<h;y++) {
        pix[yi]=0xff000000 | (dv[rsum]<<16) | (dv[gsum]<<8) | dv[bsum];
        if(x==0) {
          vmin[y]=min(y+radius+1,hm)*w;
          vmax[y]=max(y-radius,0)*w;
        }
        p1=x+vmin[y];
        p2=x+vmax[y];
        rsum+=r[p1]-r[p2];
        gsum+=g[p1]-g[p2];
        bsum+=b[p1]-b[p2];
        yi+=w;
      }
    }
  }
}